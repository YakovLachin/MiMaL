composer:
	@docker run --rm \
		-v $(CURDIR):/data \
		imega/composer \
		update --ignore-platform-reqs

tests:
	docker pull php
	@docker run --rm \
	-v $(CURDIR):/data \
	-w /data \
	php /data/vendor/bin/phpunit \
	--configuration /data/phpunit-conf.xml \
	--bootstrap /data/vendor/autoload.php \
	/data/app

build:
	# set the required into an provider argument
	vagrant up --provision --provider=lxc
.PHONY:composer build tests
