"use strict";

const gulp = require("gulp");
const del = require('del');
const typescript = require('gulp-typescript');
const tsProject = typescript.createProject('./tsconfig.json');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const cleanCSS = require('gulp-clean-css');
const template = require('gulp-template');
const htmlmin = require('gulp-htmlmin');

let dist = 'static';

// clean the contents of the distribution directory
gulp.task('clean', function () {
    return del(`${dist}/**/*`);
});

gulp.task('sass', function () {
    return gulp.src('app/views/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(dist));
});

gulp.task('compile', function (cb) {
    gulp.src('app/views/**/*.ts')
        .pipe(tsProject())
        .js.pipe(gulp.dest(dist)).on('end', cb);
});

gulp.task('template', function () {
    return gulp.src('app/views/**/*.html')
        .pipe(gulp.dest(dist));
});

gulp.task('html-min', function () {
    return gulp.src(`${dist}/**/*.html`)
        .pipe(htmlmin({collapseWhitespace: true, caseSensitive: true}))
        .pipe(gulp.dest(dist));
});

gulp.task('clean-css', function () {
    return gulp.src(`${dist}/**/*.css`)
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(dist));
});

gulp.task("concat", function () {
    return gulp.src(`${dist}/**/*.js`)
        .pipe(concat('resource.js'))
        .pipe(gulp.dest(dist));
});

gulp.task("uglify", function () {
    return gulp.src(`${dist}/**/*.js`)
        .pipe(uglify())
        .pipe(gulp.dest(dist));
});

gulp.task("watch", ['watch:ts', 'watch:html']);

gulp.task("watch:ts", function () {
    return watch('./app/views/**/*.ts', {}, function () {
        gulp.start('build');
    });
});

gulp.task("copy", function () {
    return gulp
        .src([
            'node_modules/core-js/client/shim.min.js',
            'node_modules/zone.js/dist/zone.js',
            'node_modules/reflect-metadata/Reflect.js',
            'node_modules/systemjs/dist/system.src.js',
            'node_modules/jquery/dist/jquery.min.js',
            'static/semantic.min.js',
            'systemjs.config.js'
        ])
        .pipe(uglify())
        .pipe(concat('lib.min.js'))
        .pipe(gulp.dest(dist))
});

gulp.task("svg", function () {
    return gulp
        .src('app/views/svg/**/*.svg')
        .pipe(gulp.dest(dist + '/svg'))
});

gulp.task("watch:html", function () {
    return watch('./app/views/**/*.html', {}, function () {
        gulp.start('build');
    });
});

gulp.task('build', ['compile', 'sass', 'template', 'copy', 'svg']);
gulp.task('compress', ['uglify', 'clean-css', 'html-min']);
gulp.task('default', ['build']);

