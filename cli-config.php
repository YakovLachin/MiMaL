<?php
require_once "vendor/autoload.php";

$bootstrap     = \MiMaL\Kernel::getInstance();
/**
 * @var
 */
$entityManager = $bootstrap->container("entityManager");
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

