var  util = require("util");

var phrases = {
    "hello": "Привет",
    "world": "мир"
};

function PhraseError(message) {
    this.message = message;
    Error.captureStackTrace(this, PhraseError);
}
util.inherits(PhraseError, Error);
PhraseError.prototype.name = "PhraseError";
function getPhrase(name) {
    if (!phrases[name]) {
        throw new PhraseError("Нет такой фразы:" + name);
    }

    return phrases[name];
}

function HttpError(statusCode, message) {
    this.status  = statusCode;
    this.message = message;
}

util.inherits(HttpError, Error);

function makePage(url) {
    if (url != "index.html") {
        throw new HttpError(404, "Нет такого url:" + url);
    }

    return util.format("%s, %s", getPhrase("Hello"), getPhrase("world"));
}



try {
    var page = makePage("index.html");
    console.log(page);
} catch (e) {
    if (e instanceof HttpError) {
        console.log(e.status, e.message)
    } else {
        console.error("Ошибка: %s\n Сообщение: %s\n Стек: %s", e.name, e.message, e.stack);
    }
}

