var phrases;

exports.connect = function () {
    phrases = require("./ru");
};

exports.getPhrase = function (phraseKey) {
   if (!phrases[phraseKey]) {
       throw new Error(phrases["PhraseDoesNotExist"]);
   }

   return phrases[phraseKey];
};