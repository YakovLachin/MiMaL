var db   = require("db");
db.connect();
var User = require("./user");
var log = require("logger")(module);
function run () {
    var vasya = new User("Vasya");
    var yasha = new User("Yasha");
    vasya.hello(yasha);
    yasha.hello(vasya);
    log(db.getPhrase("ConnectSuccess"));
}

if(module.parent) {
    exports.run = run;
} else {
    run();
}