<?php
//phpinfo();
//echo "Hello World!</br>";
//$memcached = new Memcached();
//$memcached->addServer("localhost", 11211) or die("no Connect");
//
//$firstname = $memcached->get('myfirstname');
//if ($firstname) {
//    echo "Hello from  memcache, " . $firstname;
//    $memcached->delete("myfirstname");
//    echo "</br>Now Meamcached would clean. You may update that page again.";
//} else {
//    $db    = new PDO("mysql:host=lo
//
//calhost;dbname=first_database", "root", "");
//    $query = $db->query("select * from first_table");
//    foreach ($query as $result) {
//
//        $res = $result["name"];
//    }
//    echo "Hello from DB Mysql, " . $res;
//    echo "</br>Please, update you page for checking Memcached.";
//    $memcached->set("myfirstname", $res);
//}

use Twig_Filter_Function;
use MiMaL\providers\Pages as Pages;
require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();
$app ['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/app/views',
));

$app['twig']->addFilter('ngValue',  new Twig_Filter_Function());
$app->get('/hello/{name}', function ($name) use ($app) {
    return $app['twig']->render('hello.html.twig', array(
        'name'=>$name
    ));
});

/**
 * Получение страницы по id.
 *
 * @param int $pageId id страницы.
 */
$app->get('/page/{pageId}', function ($pageId) use ($app) {
    $memcached = new Memcached();
    $memcached->addServer("localhost", 11211) or die("no Connect");
    $page = $memcached->get('/page/' . $pageId);
    if (!$page) {
        $db = new PDO("mysql:host=localhost;dbname=mindmaplibrary", "root", "");
        $query = $db->query("select * from pages WHERE id = $pageId");
        foreach ($query as $result) {
            $page = $result;
        }
        $memcached->set('/page/' . $pageId, $page);
    }
    if (!$page) {
        $app->abort(404, "Post does /page/$pageId not exist.");
    }

    return $app['twig']->render('pages/page.twig', $page);
});

/**
 * Получение страницы по id.
 *
 * @param int $pageId id страницы.
 */
$app->get('/upload', function () use ($app) {
    return $app['twig']->render('upload-form.twig');
});

$app->mount("/page",new Pages());
$app->run();
