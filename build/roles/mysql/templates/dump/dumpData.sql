# LOCK TABLES `entities` WRITE;
# /*!40000 ALTER TABLE `entities` DISABLE KEYS */;
# INSERT INTO `entities` VALUES
#   (1, 1, 1, 0, 'Первое содержимое', 'Первый заголовок', 1, '', '', '', '', '',
#    '2015-01-02 10:00:00');
# /*!40000 ALTER TABLE `entities` ENABLE KEYS */;
# UNLOCK TABLES;

INSERT INTO `causes` VALUES
(1, 0, 'HELLO WORLD!!!!', 'HELLO WORLD!!!! IT IS A FIRST CAUSE OF MiMaL!!!','', '',0);
INSERT INTO `causes` VALUES
(2, 0, 'HELLO, Yakov!!!!', 'HELLO WORLD mr Yakov!!!! IT IS A Second CAUSE OF MiMaL!!!','', '',0);
INSERT INTO `causes` VALUES
(3, 1, 'Oh NO, Yakov!!!!', 'Mr Yakov!!!! IT IS Cause should not to visible, because is was deleted!!!','', '',0);

INSERT INTO `users` VALUES (
  1,
  '$2y$10$.LSYhFHBhE8FNjGwUiBIieynv/T7rZVhGXEnOFeah00ERUS9VfKKq',
  '123@mail.ru',
  0,
  '2017-04-13 20:19:33',
  '2017-04-13 20:19:33'
);

INSERT INTO `user_to_cause_relations` VALUES
(1, 1);
INSERT INTO `user_to_cause_relations` VALUES
(1, 2);
INSERT INTO `user_to_cause_relations` VALUES
(1, 3);

INSERT INTO `effects` VALUES
  (1, 0, 'HELLO WORLD!!!!', 'HELLO WORLD!!!! IT IS A FIRST EFFECT OF MiMaL!!!','', '',0);

INSERT INTO `cause_to_effect_relations` VALUES
  (1, 1);

INSERT INTO `effect_to_cause_relations` VALUES
  (1, 2);
