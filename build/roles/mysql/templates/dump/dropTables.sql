DROP TABLE IF EXISTS `pages`;
DROP TABLE IF EXISTS `cause_to_effect_relations`;
DROP TABLE IF EXISTS `effect_to_cause_relations`;
DROP TABLE IF EXISTS `user_to_cause_relations`;
DROP TABLE IF EXISTS `user_to_effect_relations`;
DROP TABLE IF EXISTS `relations`;
DROP TABLE IF EXISTS `causes`;
DROP TABLE IF EXISTS `effects`;
DROP TABLE IF EXISTS `users`;
