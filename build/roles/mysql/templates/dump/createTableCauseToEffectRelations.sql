SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
;
CREATE TABLE `cause_to_effect_relations` (
  `cause_id`  INT(10)      NOT NULL,
  `effect_id` INT(10)      NOT NULL,
  FOREIGN KEY (cause_id) REFERENCES causes (cause_id),
  FOREIGN KEY (effect_id) REFERENCES effects (effect_id)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
