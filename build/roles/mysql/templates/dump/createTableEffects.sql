DROP TABLE IF EXISTS `effects`;
CREATE TABLE `effects` (
  `effect_id`    INT(5)                  NOT NULL AUTO_INCREMENT,
  `is_deleted`   TINYINT(1)              NOT NULL DEFAULT 0,
  `title`        VARCHAR(255)
                 COLLATE utf8_unicode_ci NOT NULL,
  `content`      TEXT
                 COLLATE utf8_unicode_ci NOT NULL,
  `link`         VARCHAR(255)
                 COLLATE utf8_unicode_ci          DEFAULT NULL,
  `preview_link` VARCHAR(255)
                 COLLATE utf8_unicode_ci          DEFAULT NULL,
  `date_created` INT                              DEFAULT NULL,
  PRIMARY KEY (`effect_id`),
  KEY (`title`)
)

ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARSET = utf8
COLLATE = utf8_unicode_ci;
