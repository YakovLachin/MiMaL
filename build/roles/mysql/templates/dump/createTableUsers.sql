SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;

CREATE TABLE `users` (
  `user_id`            INT(11)                 NOT NULL AUTO_INCREMENT,
  `password`           VARCHAR(255)            NOT NULL
  COLLATE utf8_unicode_ci,
  `email`              VARCHAR(256)
                       COLLATE utf8_unicode_ci NOT NULL,
  `type`               INT(11)                 NOT NULL,
  `registrationDate`   VARCHAR(255)
                       COLLATE utf8_unicode_ci          DEFAULT NULL,
  `dateExpiresPremium` VARCHAR(255)
                       COLLATE utf8_unicode_ci          DEFAULT NULL,
  PRIMARY KEY (`user_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;