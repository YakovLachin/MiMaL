SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
;
CREATE TABLE `effect_to_cause_relations` (
  `effect_id` INT(10)      NOT NULL,
  `cause_id`  INT(10)      NOT NULL,
  FOREIGN KEY (effect_id) REFERENCES effects (effect_id),
  FOREIGN KEY (cause_id) REFERENCES causes (cause_id)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
