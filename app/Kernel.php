<?php

namespace MiMaL;

use Firebase\JWT\JWT;
use MiMaL\Core\Relation\RelationPimpleProvider;
use MiMaL\Core\SecureLink\SecureLinkPimpleProvider;
use MiMaL\Encryption\Crypter;
use MiMaL\Services\DataBase;
use MiMaL\Core\Security\SecurityService;
use MiMaL\Services\Site;
use MiMaL\Core\User\UserService;
use MiMaL\Validation\Provider as Validator;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Класс загрузчик Требуемых зависимостей.
 *
 * Class Bootstrap
 * @package MiMal
 */
class Kernel implements ServiceProviderInterface
{
    /**
     * @see \MiMaL\Services\Site
     */
    const KERNEL_SITE = 'serviceSite';

    /**
     * @see \MiMaL\Services\DataBase
     */
    const KERNEL_DATA_BASE_SERVICE = 'dataService';

    /**
     * @see \MiMaL\Encryption\Crypter
     */
    const CRYPTER                  = 'crypter';
    const CRYPTER_SECRET_KEY       = 'crypter.secret.key';

    protected static $instance;
    protected $container;
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        /**
         * Pimple is a small Dependency Injection Container for PHP.
         * @link http://pimple.sensiolabs.org/
         */

        /**
         * Возвращает класс для работы с Memcache
         * @link  http://php.net/manual/ru/class.memcached.php
         *
         * @param $c    Kлюч  обязательный ключ для Pimple.
         *              Вводится при каждом объявления контейнера функции.
         *
         * @return \Memcached
         */
        $pimple['memcached'] = function ($c) {
            $memcached = new \Memcached();
            $memcached->addServer("localhost", 11211) or die("no Connect");
            return $memcached;
        };

        /**
         * Возвращает класс для работы с Базой данных.
         * @link   http://php.net/manual/ru/book.pdo.php
         *
         * @param $c    Обязательный ключ для Pimple.
         *              Вводится при каждом объявления контейнера функции.
         *
         * @return \PDO
         */
        $pimple['PDO'] = function ($c) {
            $options = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'');
            $pdo     = new \PDO("mysql:host=localhost;dbname=MiMaL", "root","", $options);
            return $pdo;
        };

        /**
         * Возвращает ORM manager Doctrine
         *
         * @link http://doctrine-orm.readthedocs.io
         *
         * @param $c
         *
         * @return \Doctrine\ORM\EntityManager
         */
        $pimple['entityManager'] = function ($c) {
            $isDevMode  = true;

            $setup  = new \Doctrine\ORM\Tools\Setup;
            $config = $setup->createAnnotationMetadataConfiguration(array(__DIR__ . '/Models'), $isDevMode);
            $connectionParams = array(
                'dbname'   => 'MiMaL',
                'user'     => 'root',
                'password' => '',
                'host'     => 'localhost',
                'driver'   => 'pdo_mysql',
                'charset'  => 'utf8'
            );

            $entityManager =  \Doctrine\ORM\EntityManager::create($connectionParams, $config);

            return $entityManager;
        };

        /**
         *  Возвращает с конфигурируемый Twig
         *
         * @param $c
         * @return \Twig_Environment
         */
        $pimple['twig'] = function ($c) {
            $path   = __DIR__ . "/views";
            $loader = new \Twig_Loader_Filesystem(array('twig.path' => $path));
            $twig   = new \Twig_Environment($loader);

            return $twig;
        };

        /**
         * Сервис для работы с базой данных.
         *
         * @param $c
         * @return DataBase
         */
        $pimple[self::KERNEL_DATA_BASE_SERVICE] = function ($c) {
            $manager = $c["entityManager"];
            $dataService = DataBase::getInstance($manager);

            return $dataService;
        };


        /**
         * Сервис для работы с токенами.
         * Требуется для аунтификации пользователя.
         *
         * @param $c
         * @return SecurityService
         */
        $pimple["serviceSecurity"] = function($c) {
            /**
             * @var Site $serviceSite
             */
            $serviceSite = $c["serviceSite"];
            $config      = $c["config"];
            $secretKey   = $config["serviceSecurity"]["secretKey"];
            /**
             * @var UserService $serviceUsers
             */
            $serviceUsers = $c["user.service"];

            /**
             * @var Crypter $crypter
             */
            $crypter = $c[self::CRYPTER];

            return new SecurityService($secretKey, $serviceSite, $serviceUsers, $crypter);
        };

        /**
         * Сервис сайта.
         * Хранит информацию о текущем пользователе.
         *
         * @param $c
         *
         * @return Site
         */
        $pimple[self::KERNEL_SITE] = function($c) {
            return  Site::getInstance();
        };

        /**
         * @param Container $c
         *
         * @return \MiMal\Encryption\Crypter
         */
        $pimple[self::CRYPTER] = function ($c) {
            $jwt = new JWT();
            return new  \MiMaL\Encryption\Crypter($jwt, $c->offsetGet(self::CRYPTER_SECRET_KEY));
        };

        /**
         * @param Container $pimple
         *
         * @return string
         */
        $pimple[self::CRYPTER_SECRET_KEY] = function ($pimple) {
            try {
                $config = $pimple["config"];
                $secretKey = $config["serviceSecurity"]["secretKey"];
            } catch (\Exception $e) {
                $secretKey = "WARNING_CRYPTER_SECKRET_KEY_IS_NOT_DEIFNED_DMSOcsidvjs1123";
            }

            return $secretKey;
        };

        /**
         * Конфигурация приложения.
         *
         * @param $c
         *
         * @return mixed
         */
        $pimple["config"] = function($c) {
            $config = require_once __DIR__ . "/config/config.php";

            return $config;
        };

        $pimple->register(new Validator());
        $pimple->register(new SecureLinkPimpleProvider());
        $pimple->register(new RelationPimpleProvider());
    }
}
