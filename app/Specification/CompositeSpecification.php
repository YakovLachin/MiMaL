<?php

namespace MiMaL\Specification;

abstract class CompositeSpecification implements SpecificationInterface
{
    abstract function isSatisfiedBy($item);

    public function andSpecification(SpecificationInterface $specification)
    {
        return new AndSpecification($this, $specification);
    }

    public function orSpecification(SpecificationInterface $specification)
    {
        return new OrSpecification($this, $specification);
    }

    public function notSpecification()
    {
        return new NotSpecification($this);
    }

    public function andNotSpecification(SpecificationInterface $specification) {
        return new AndNotSpecification($this, $specification);
    }
}
