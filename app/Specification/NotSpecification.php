<?php

namespace MiMaL\Specification;

/**
 * Class NotSpecification
 * @package Nethouse\Resolver\Specification
 */
class NotSpecification extends CompositeSpecification
{
    /**
     * @var SpecificationInterface
     */
    private $specification;

    public function __construct(SpecificationInterface $specification)
    {
        $this->specification = $specification;
    }

    public function isSatisfiedBy($item)
    {
        return !$this->specification->isSatisfiedBy($item);
    }
}
