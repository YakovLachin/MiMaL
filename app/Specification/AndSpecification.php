<?php

namespace MiMaL\Specification;

class AndSpecification extends  CompositeSpecification
{
    /**
     * @var SpecificationInterface[]
     */
    private $specifications;

    /**
     * @param SpecificationInterface[] ...$specifications
     */
    public function __construct(SpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param $item
     *
     * @return bool
     */
    public function isSatisfiedBy($item)
    {
        $satisfied = [];

        foreach ($this->specifications as $specification) {
            $satisfied[] = $specification->isSatisfiedBy($item);
        }

        return !in_array(false, $satisfied);
    }

}
