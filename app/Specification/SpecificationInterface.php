<?php

namespace MiMaL\Specification;

/**
 * Interface SpecificationInterface
 * @package Nethouse\Resolver\Specification
 */
interface SpecificationInterface
{
    public function isSatisfiedBy($item);
    public function andSpecification(SpecificationInterface $specification);
    public function orSpecification(SpecificationInterface $specification);
    public function andNotSpecification(SpecificationInterface $specification);
    public function notSpecification();
}
