<?php

namespace MiMaL\Specification;

class AndNotSpecification extends CompositeSpecification
{
    /**
     * @var SpecificationInterface
     */
    protected $left;

    /**
     * @var SpecificationInterface
     */
    protected $right;

    /**
     * AndNotSpecification constructor.
     *
     * @param $left
     * @param $right
     */
    public function __construct(SpecificationInterface $left, SpecificationInterface$right)
    {
        $this->left  = $left;
        $this->right = $right;
    }

    function isSatisfiedBy($item)
    {
        return $this->left->isSatisfiedBy($item) &&
               !$this->right->isSatisfiedBy($item);
    }
}
