<?php

namespace MiMaL\Services;

use Doctrine\ORM\EntityManager;
use MiMaL\Models\DefaultModel;
use \MiMaL\Models\Types;

/**
 * Class DataBase
 * @package MiMaL\managers
 */
class DataBase
{

    protected static $instance;
    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @var mixed[] $repositories
     */
    protected static $repositories;

    /**
     *  @param EntityManager $entityManager
     */
    protected function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->setMatchesTypesAndRepositories();
        return $this;
    }

    /**
     * @param EntityManager $entityManager
     *
     * @return DataBase
     */
    public static function getInstance(EntityManager $entityManager)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($entityManager);
        }

        return self::$instance;
    }

    /**
     * Устанвливает соотношение типов и моделей сущностей.
     */
    protected function setMatchesTypesAndRepositories()
    {
        self::$repositories = array(
            Types::USER   => \MiMaL\Core\User\UserModel::class,
            Types::CAUSE  => \MiMaL\Core\Cause\CauseModel::class,
            Types::EFFECT => \MiMaL\Core\Effect\EffectModel::class,
            Types::CAUSE_TO_EFFECT_RELATION => \MiMaL\Core\Relation\CauseEffectRelation::class,
            Types::EFFECT_TO_CAUSE_RELATION => \MiMaL\Core\Relation\EffectCauseRelation::class,
            Types::USER_TO_CAUSE_RELATION   => \MiMaL\Core\Relation\UserCauseRelation::class,
            Types::USER_TO_EFFECT_RELATION  => \MiMaL\Core\Relation\UserEffectRelation::class,
        );
    }


    /**
     * @param $entity
     */
    public function writeEntity($entity)
    {
        $manager = $this->entityManager;
        $manager->persist($entity);
        $manager->flush();

        return $entity;
    }

    /**
     * Чтение объекта по его типу и требованиям.
     *
     * @param int    $entityType Тип требуемого объекта.
     * @param array  $conditions Параметры передаваемые по данному объекту.
     *
     * @return DefaultModel
     */
    public function readEntity($entityType, $conditions = array())
    {
        $entity = null;
        if (!empty($this->entityManager)) {
            try {
            /**
             * @var DefaultModel $entity
             */
            $entity = $this->entityManager->getRepository(self::$repositories[$entityType])
            ->findOneBy($conditions);
                } catch (\Exception $exception) {
                echo $exception->getMessage();
            };
        }

        return $entity;
    }

    /**
     * Метод предназначен для отображения шаблона.
     *
     * @param $pathToTemplate
     * @param $context
     */
    public function renderTemplate($pathToTemplate, $context = array())
    {
        $twig = $this->diContainer('twig');
        return $twig->render($pathToTemplate, $context);
    }
}
