<?php

namespace MiMaL\Services;

use MiMaL\Core\User\UserModel;

class Site {
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var UserModel $user
     */
    protected $user;

    /**
     * @var string
     */
    protected $userToken;

    /**
     * @return Site
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param UserModel $user
     */
    public function setUser(UserModel $user)
    {
        $this->user = $user;
    }

    /**
     * Возвращает текущего пользователя.
     *
     * @return UserModel
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setUserToken($token) {
        $this->userToken = $token;
    }

    public function getUserToken() {
        return $this->userToken;
    }
}
