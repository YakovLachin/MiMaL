<?php

namespace MiMaL\Services;

use MiMaL\Core\User\UserModel;
use MiMaL\Core\User\UserService;
use Firebase\JWT\JWT;
use MiMaL\Errors\Users\AuthorizeError;


class Security {

    protected static $instance;

    /**
     * Секретный ключ для шифровки расшифровки токена
     * @var string
     */
    private $secretKey;

    /**
     * Сервис сайта.
     * @var Site
     */
    private $serviceSite;

    /**
     * Сервис пользователей.
     * @var UserService
     */
    private $serviceUsers;

    /**
     * @param string $secretKey    Секретный ключ для шифровкии расшифровки токена.
     * @param Site   $serviceSite  Сервис сайта.
     * @param UserService  $serviceUsers Сервис пользователей.
     *
     * @internal param Users $usersService
     */
    protected function __construct($secretKey, $serviceSite, $serviceUsers)
    {
        $this->secretKey    = $secretKey;
        $this->serviceSite  = $serviceSite;
        $this->serviceUsers = $serviceUsers;
    }

    /**
     * @param string $secretKey    Секретный ключ для шифровкии расшифровки токена.
     * @param Site   $serviceSite  Сервис сайта.
     * @param UserService  $serviceUsers Сервис пользователей.
     *
     * @return Security
     */
    public static function getInstance($secretKey, Site $serviceSite, UserService $serviceUsers)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($secretKey, $serviceSite, $serviceUsers);
        }

        return self::$instance;
    }

    /**
     * Возвращает токен по входящим данным.
     *
     * @param mixed[] $data
     * @return string
     */
    public function token($data)
    {
        $date = new \DateTime('now');
        $expired = $date->modify('+1 day');
        $token = array(
            "exp"  => $expired->getTimestamp(),
            "data" => $data
        );

        $jwt = JWT::encode($token, $this->secretKey);

        return $jwt;
    }

    /**
     * Возвращает данные из токена.
     *
     * @param $jwt
     * @return mixed[]
     */
    protected function dataFromjwt($jwt) {
        $data = JWT::decode($jwt, $this->secretKey, array('HS256'));
        return $data->data;
    }

    /**
     * Аунтифицирует по токену.
     *
     * @param $token
     * @return bool
     * @throws \MiMal\Errors\Users\AuntificationError
     */
    public function auntificateByToken($token) {
        $data  = get_object_vars($this->dataFromjwt($token));
        $condition = array(
            "id" => $data["id"]
        );


        $user = $this->serviceUsers->userByCondition($condition);
        if ($user) {
            $this->serviceSite->setUser($user);
        } else {
            throw new \MiMal\Errors\Users\AuntificationError("User not Found");
        }
        $token = $this->token($data);
        $this->serviceSite->setUserToken($token);

        return true;
    }

    /**
     * Авторизирует польователя по email и паролю.
     *
     * @param $email
     * @param $password
     *
     * @return UserModel
     *
     * @throws AuthorizeError
     */
    public function authorize($email, $password)
    {
        $condition = array(
            "email" => $email
        );

        /**
         * @var UserModel $user
         */
        $user = $this->serviceUsers->UserByCondition($condition);
        if ($user && $user->checkPassword($password)) {
            $this->setCurrentUser($user);
        } else {
            throw new AuthorizeError("Логин или пароль - не действительны.");
        }
    }

    public function registrate($email, $password) {
        /**
         * @var UserModel $user
         */
        $user = $this->serviceUsers->createUser($email, $password);
        if ($user) {
            $this->setCurrentUser($user);
        } else {
            throw new AuthorizeError("Ошибка при регистрации.");
        }
    }

    /**
     * @param UserModel $user
     */
    protected function setCurrentUser(UserModel $user) {
        $this->serviceSite->setUser($user);
        $fields = $user->toArray();
        $data   = array(
            "id" => $fields["id"]
        );
        $token  = $this->token($data);
        $this->serviceSite->setUserToken($token);
    }
}

