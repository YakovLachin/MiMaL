<?php

namespace MiMaL\Services;

/**
 * Class Pages
 * Сервис для  работы со страницами.
 * @package MiMaL\managers
 */
class Pages
{
    /**
     * @var \MiMaL\Services\DataBase $dataBaseService
     */
    protected $dataBaseService;

    /**
     * @var \Twig_Environment $renderer
     */
    protected $renderer;


    /**
     * @param \MiMaL\Services\DataBase $dataBaseService Сервис для работы с БД.
     * @param \Twig_Environment        $renderer        Рендерер Твиг.
     */
    public function __construct(\MiMaL\Services\DataBase $dataBaseService, $renderer)
    {
        $this->dataBaseService = $dataBaseService;
        $this->renderer        = $renderer;
    }

    /**
     * Отображает страницу по её id;
     * @param $id
     */
    public function pageById($id)
    {
        $conditions     = array(
            "id"         => $id,
            "typeEntity" => \MiMaL\Models\DefaultModel::PAGE,
        );
        /**
         * @var \MiMaL\Models\Page $page
         */
        $page = $this->dataBaseService->readEntity(\MiMaL\Models\DefaultModel::PAGE, $conditions, "page/$id");
        $pathToTemplate = "/pages/page.twig";
        $result         = $this->renderer->render($pathToTemplate, $page->toArray());

        return $result;
    }

    public function create()
    {}

    public function update()
    {}
}
