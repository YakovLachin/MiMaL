import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from "@angular/http";

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { CauseComponent } from "./cause/cause.component";
import { UserComponent } from "./user/user.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CauseService } from "./cause/cause.service";
import { UserService } from "./user/user.service";
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { AppRoutingModule } from "./app-routing.module";
import { EffectService } from "./effect/effect.service";
import { EffectComponent } from "./effect/effect.component";
import { SignupComponent } from "./signup/signup.component";
import {SignupService} from "./signup/signup.service";
import {SigninService} from "./signin/signin.service";
import {SigninComponent} from "./signin/signin.component";
// import { SuiModule } from "ng2-semantic-ui";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule
        // SuiModule
    ],
    declarations: [
        AppComponent,
        CauseComponent,
        EffectComponent,
        UserComponent,
        SignupComponent,
        SigninComponent,
        DashboardComponent
    ],
    providers: [
        CauseService,
        EffectService,
        UserService,
        SignupService,
        SigninService,
        CookieService
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule {}
