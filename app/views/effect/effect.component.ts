import {Component, OnInit} from '@angular/core';
import {Effect}           from './effect';
import {EffectService}    from './effect.service';
import {ActivatedRoute, Params} from "@angular/router";
import {Location}                 from '@angular/common';
import {Observable} from 'rxjs';
import  'rxjs/add/operator/switchMap';
import {isUndefined} from "util";
@Component({
    moduleId: module.id,
    selector: 'effect',
    templateUrl: 'effect.component.html',
    providers: [EffectService]
})


/**
 * компонент следствия
 * @class EffectComponent
 */
export class EffectComponent implements OnInit {
    abstract;
    /**
     * @memberOf EffectComponent
     * @property {string}
     */
    title = 'Simple Title';
    effects: Effect[];
    effect: Effect;
    editEffect: boolean;

    constructor(private effectService: EffectService,
                private route: ActivatedRoute,
                private location: Location) {
    }

    /**
     * Получает и синхронизирует из бекенда полученый список
     * cледствий с effects
     * @todo переделать на список родительских и дочерних  списки\
     * Следствий (Effect[]).
     */
    getEffects(): void {
        this.effectService.getEffects().then(effects => this.effects = effects.slice(1,5));
    }

    /**
     * Получает и синхронизирует из бекенда 
     * следствие по идентификатору с первой причиной (effect)
     */
    getEffect(): void {
        this.route.params
            .switchMap((params: Params) => this.effectService.getEffect(+params['id']))
            .subscribe(effect => this.effect = effect);
    }

    /**
     * Сохраняет текущие следствие.
     */
    save(): void {
        this.effectService.update(this.effect).
            then(() => this.goBack());
    }

    /**
     * Добавляет следствие.
     *
     * @param title
     */
    add(title: string): void {
        title = title.trim();
        if (!title) { return; }
        this.effectService.create(title).
            then(effect => {
                this.effects.push(effect);
                this.effect = null;
        });
    }

    /**
     * Удаляет следствие.
     *
     * @param effect
     */
    delete(effect: Effect): void {
        this.effectService.delete(effect)
            .then(() => {
                this.effects = this.effects.filter(c => c !== effect);
                if (this.effect === effect) {this.effect = null;}
            });
    }

    /**
     * Инициализация класса
     */
    ngOnInit(): void {
        this.getEffect();
        this.editEffect = false;
        // this.getEffects();
    }

    /**
     * Возвращает на последию посещеную страницу.
     */
    goBack(): void {
        this.location.back();
    }

    /**
     * Синхронизирует selectedEffect с выбранным Effect
     *
     * @param effect
     */
    onSelect(effect: Effect): void {
        this.effect = effect;
    }

    hasCapability(key: string): boolean {
        let capabilities  = this.effect.capabilities;
        for (let capability of capabilities ) {
            if (capability.rel == key) {
                return true;
            }
        }

        return false;
    }
}


