import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Effect } from './effect';
import  'rxjs/add/operator/toPromise';
import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class EffectService {
    private effectUrl = 'api/effect';
    private headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http) {
    }

    /**
     * Возвращает список "Следствий"
     * @todo Переписать на список родительских или дочерних "Следствий" (Effect[])
     * по идентификатору Следствия.
     *
     * @returns {Promise<Effect[]> }
     */
    getEffects():Promise<Effect[]> {
        return this.http.get(this.effectUrl, { headers: this.headers })
            .toPromise()
            .then(response => response.json().data as Array<Effect>)
            .catch(this.handleError);
    }
    /**
     * Возвращает  причину по её идентификатору.
     *
     * @param id
     * @returns {Promise<Effect>}
     */
    getEffect(id:number):Promise<Effect> {
        const url = `${this.effectUrl}/${id}`;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => response.json().data as Effect)
            .catch(this.handleError);
    }

    /**
     * Обрабатывает возможную ошибку.
     *
     * @param error
     * @returns {any}
     */
    private handleError(error:any):Promise<any> {
        console.error('An error ocurred', error);

        return Promise.reject(error.message || error);
    }

    update(firstEffect: Effect):Promise<Effect> {
        const url = `${this.effectUrl}/${firstEffect.id}`;
        return this.http
            .put(url, JSON.stringify(firstEffect), {headers: this.headers})
            .toPromise()
            .then(() => firstEffect)
            .catch(this.handleError);
    }

    create(title: string): Promise<Effect> {
        return this.http
            .post(this.effectUrl, JSON.stringify({title:title}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(effect: Effect): Promise<void> {
        const url = `${this.effectUrl}/${effect.id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
