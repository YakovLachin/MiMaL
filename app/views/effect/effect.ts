import {Cause} from "../cause/cause";
import {Capability} from "../capability";
/**
 * Сущность следствия.
 *
 * @class Effect
 */
export class Effect {
    /**
     * Уникальный иденьтификатор.
     *
     * @memberOf Effect
     * @property {int}
     */
    id: number;
    /**
     * Заголовок.
     *
     * @memberOf Effect
     * @property {string}
     */
    title: string;

    /**
     * Содержимое.
     *
     * @memberOf Effect
     * @property {string}
     */
    content: string;

    /**
     * Ссылка на превью.
     *
     * @memberOf Effect
     * @property {string}
     */
    previewLink: string;


    /**
     * Дата создания.
     *
     * @memberOf Effect
     * @property {number}
     */
    dateCreated: number;

    /**
     * Ссылка на более подробную информацию.
     *
     * @memberOf Effect
     * @property {string}
     */
    link: string;

    /**
     * Родительский следствия.
     *
     * @memberOf Effect
     * @property {Cause[]}
     */
    ancestors: Cause[];

    /**
     * Дочерние следствия.
     *
     * @memberOf Effect
     * @property {Cause[]}
     */
    descendants: Cause[];

    /**
     * Доступные действия над объектом.
     *
     * @memberOf Effect
     * @property {Capability[]}
     */
    capabilities: Capability[];
}
