import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CauseComponent } from './cause/cause.component';
import { UserComponent } from "./user/user.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { EffectComponent } from "./effect/effect.component";
import { SignupComponent } from "./signup/signup.component";
import { SigninComponent } from "./signin/signin.component";

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'cause/:id', component: CauseComponent },
    { path: 'effect/:id', component: EffectComponent },
    { path: 'user/:id', component: UserComponent},
    { path: 'dashboard', component: DashboardComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'signin', component: SigninComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [ RouterModule ]
}
)

export class AppRoutingModule {}