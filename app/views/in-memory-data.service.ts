import { InMemoryDbService } from "angular-in-memory-web-api";
export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let cause = [
            {id: 1, title:  "title7", content: "content7", previewLink: "link7"},
            {id: 8, title:  "title8", content: "content8", previewLink: "link8"},
            {id: 9, title:  "title9", content: "content9", previewLink: "link9"},
            {id: 10, title: "title10", content: "content10", previewLink: "link10"},
            {id: 11, title: "title11", content: "content11", previewLink: "link11"},
            {id: 12, title: "title12", content: "content12", previewLink: "link12"},
            {id: 13, title: "title13", content: "content13", previewLink: "link13"},
        ];
        return { cause }
    }

}
