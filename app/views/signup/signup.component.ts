///<reference path="../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>
import {Component, OnInit} from '@angular/core';
import {User}           from './user';
import {SignupService}    from './signup.service';
import {Observable} from 'rxjs';
import  'rxjs/add/operator/switchMap';
 import {isUndefined} from "util";
@Component({
    moduleId: module.id,
    selector: 'signup',
    templateUrl: 'signup.component.html',
    providers: [SignupService]
})


/**
 * компонент регистрации
 * @class SignupComponent
 */
export class SignupComponent implements OnInit {
    abstract;

    /**
     * @memberOf SignupComponent
     * @property {string}
     */
    title = 'Simple Title';
    user: User;
    editCause: boolean;
    constructor(private signupService: SignupService) {
    }

    /**
     * Регистрирует текущего пользователя.
     */
    signup(): void {
        console.log(this.user);
        this.signupService.signup(this.user).
            then(user => this.user = user);
    }

    ngOnInit(): void {
        this.user = new User();
    }
}


