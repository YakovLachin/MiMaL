import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from './user';
import  'rxjs/add/operator/toPromise';
import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class SignupService {
    private url = 'api/signup';
    private headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http) {
    }
    
    /**
     * Обрабатывает возможную ошибку.
     *
     * @param error
     * @returns {any}
     */
    private handleError(error:any):Promise<any> {
        console.error('An error ocurred', error);

        return Promise.reject(error.message || error);
    }

    signup(user: User):Promise<User> {
        const url = `${this.url}`;
        return this.http
            .post(url, JSON.stringify(user), {headers: this.headers})
            .toPromise()
            .then(() => user)
            .catch(this.handleError);
    }

}
