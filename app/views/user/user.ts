
import { Cause } from "../cause/cause";
export class User {
    /**
     * Уникальный иденьтификатор.
     *
     * @memberOf User
     * @property {int}
     */
    id: number;

    /**
     * Электронная почта пользователя.
     *
     * @memberOf User
     * @property {int}
     */
    email: string;

    /**
     * Список причин прикрепленных к пользователю.
     *
     * @memberOf User
     * @property {Cause[]}
     */
    causes: Cause[];
}