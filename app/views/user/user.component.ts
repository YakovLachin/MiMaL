import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Location}                 from '@angular/common';
import {UserService} from './user.service';
import {User} from "./user";
@Component({
    moduleId: module.id,
    selector: 'user',
    templateUrl: 'user.component.html',
    providers: [UserService]
})

export class UserComponent implements OnInit {
    abstract;
    /**
     * @memberOf UserComponent
     * @property {string}
     */
    title = 'Simple Title';
    user: User;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private location: Location) {
    }


    getUser(): void {
        this.route.params
            .switchMap((params: Params) => this.userService.getUser(+params['id']))
            .subscribe(user => this.user = user);
    }

    ngOnInit(): void {
        this.getUser();
    }

    /**
     * Возвращает на последию посещеную страницу.
     */
    goBack(): void {
        this.location.back();
    }
}