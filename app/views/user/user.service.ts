import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from './user';


@Injectable()
export class UserService {
    private userUrl = 'api/user';
    private headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http) {
    }

    /**
     * Возвращает пользователя по его идентиыикатору.
     *
     * @param id
     * @returns {Promise<User>}
     */
    getUser(id:number):Promise<User> {
        const url = `${this.userUrl}/${id}`;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => response.json().data as User)
            .catch(this.handleError);

    }

    private handleError(error:any):Promise<any> {
        console.error('An user Service Error ocured', error);

        return Promise.reject(error.message || error);
    }
}
