import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PageComponent } from "./page.component";

@NgModule({
    imports:      [ BrowserModule ],
    declarations: [PageComponent],
    bootstrap: [PageComponent]
})
export class PageModule { }