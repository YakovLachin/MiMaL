
import {Component, OnInit} from '@angular/core';
import {User}           from './user';
import {SigninService}    from './signin.service';
import {Observable} from 'rxjs';
import  'rxjs/add/operator/switchMap';
 import {isUndefined} from "util";
@Component({
    moduleId: module.id,
    selector: 'signin',
    templateUrl: 'signin.component.html',
    providers: [SigninService]
})


/**
 * компонент атворизации
 * @class SigninComponent
 */
export class SigninComponent implements OnInit {
    abstract;

    /**
     * @memberOf SigninComponent
     * @property {string}
     */
    title = 'Simple Title';
    user: User;
    editCause: boolean;
    constructor(private signinService: SigninService) {
    }

    /**
     * Авторизируетё текущего пользователя.
     */
    signin(): void {
        console.log(this.user);
        this.signinService.signin(this.user).
            then(user => this.user = user);
    }

    ngOnInit(): void {
        this.user = new User();
    }
}


