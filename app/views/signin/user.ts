export class User {
    /**
     * Электронная почта пользователя.
     *
     * @memberOf User
     * @property {int}
     */
    email: string;

    /**
     * Пароль пользователя.
     *
     * @memberOf User
     * @property {int}
     */
    password: string;
}