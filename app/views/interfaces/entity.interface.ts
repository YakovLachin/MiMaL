export interface EntityInterface
{
    /**
     * Уникальный иденьтификатор.
     *
     *
     * @memberOf Cause
     * @property {int}
     */
    id: number;
    /**
     * Заголовок.
     *
     * @memberOf Cause
     * @property {string}
     */
    title: string;

    /**
     * Содержимое.
     *
     * @memberOf Cause
     * @property {string}
     */
    content: string;

    /**
     * Ссылка на превью.
     *
     * @memberOf Cause
     * @property {string}
     */
    previewLink: string;
}