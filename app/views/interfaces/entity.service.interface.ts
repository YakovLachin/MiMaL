import {EntityInterface} from './entity.interface';


export interface EntityServiceInterface
{
    getChilds(entity: EntityInterface);
    getParents(entity: EntityInterface);
    update(entity: EntityInterface);
    create(title: string);
    remove(entity: EntityInterface);
    read(id: number);
    handleError(error: any);
}
