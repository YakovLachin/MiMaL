export class Capability {
    /**
     * Разрешенное действие.
     *
     * @memberOf Capability
     * @property {string}
     */
    rel: string;

    /**
     * Метод запроса.
     *
     * @memberOf Capability
     * @property {string}
     */
    method: string;

    /**
     * Ссылка на действие.
     *
     * @memberOf Capability
     * @property {string}
     */
    link: string;
}