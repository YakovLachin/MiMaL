import {Component, OnInit} from '@angular/core';
import {Cause}           from './cause';
import {CauseService}    from './cause.service';
import {ActivatedRoute, Params} from "@angular/router";
import {Location}                 from '@angular/common';
import {Observable} from 'rxjs';
import  'rxjs/add/operator/switchMap';
 import {isUndefined} from "util";
@Component({
    moduleId: module.id,
    selector: 'cause',
    // template: '<h1>{{title}}</h1>' +
    // '<a routerLink="/heroes">Heroes</a>' +
    // '<router-outlet></router-outlet>',

    templateUrl: 'cause.component.html',
    providers: [CauseService]
})


/**
 * компонент причины
 * @class CauseComponent
 */
export class CauseComponent implements OnInit {
    abstract;
    /**
     * @memberOf CauseComponent
     * @property {string}
     */
    title = 'Simple Title';
    causes: Cause[];
    cause: Cause;
    editCause: boolean;

    constructor(private causeService: CauseService,
                private route: ActivatedRoute,
                private location: Location) {
    }

    /**
     * Получает и синхронизирует из бекенда полученый список
     * причин с causes
     * @todo переделать на список родительских и дочерних  списки\
     * Следствий (Effect[]).
     */
    getCauses(): void {
        this.causeService.getCauses().then(causes => this.causes = causes.slice(1,5));
    }

    /**
     * Получает и синхронизирует из бекенда полученую
     * причину по идентификатору с первой причиной (cause)
     */
    getCause(): void {
        this.route.params
            .switchMap((params: Params) => this.causeService.getCause(+params['id']))
            .subscribe(cause => this.cause = cause);
    }

    /**
     * Сохраняет текущую причину.
     */
    save(): void {
        console.log(this.cause);
        this.causeService.update(this.cause).
            then(cause => this.cause = cause);
    }

    /**
     * Добавляет причину.
     *
     * @param title
     */
    add(title: string): void {
        title = title.trim();
        if (!title) { return; }
        this.causeService.create(title).
            then(cause => {
                this.causes.push(cause);
                this.cause = null;
        });
    }

    /**
     * Удаляет причину.
     *
     * @param cause
     */
    delete(cause: Cause): void {
        this.causeService.delete(cause)
            .then(() => {
                this.causes = this.causes.filter(c => c !== cause);
                if (this.cause === cause) {this.cause = null;}
            });
    }

    /**
     * Инициализация класса
     */
    ngOnInit(): void {
        this.getCause();
        this.editCause = false;
        // this.getCauses();
    }

    /**
     * Возвращает на последию посещеную страницу.
     */
    goBack(): void {
        this.location.back();
    }

    /**
     * Синхронизирует cause с выбранным Cause
     *
     * @param cause
     */
    onSelect(cause: Cause): void {
        this.cause = cause;
    }

    hasCapability(key: string): boolean {
        let capabilities  = this.cause.capabilities;
        for (let capability of capabilities ) {
             if (capability.rel == key) {
                 return true;
             }
        }

        return false;
    }
}


