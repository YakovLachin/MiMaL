import {Effect} from "../effect/effect";
import {Capability} from "../capability";

/**
 * Сущность причины.
 *
 * @class Cause
 */
export class Cause {
    /**
     * Уникальный иденьтификатор.
     *
     * @memberOf Cause
     * @property {int}
     */
    id: number;
    /**
     * Заголовок.
     *
     * @memberOf Cause
     * @property {string}
     */
    title: string;

    /**
     * Содержимое.
     *
     * @memberOf Cause
     * @property {string}
     */
    content: string;

    /**
     * Ссылка на превью.
     *
     * @memberOf Cause
     * @property {string}
     */
    previewLink: string;


    /**
     * Дата создания.
     *
     * @memberOf Cause
     * @property {number}
     */
    dateCreated: number;

    /**
     * Ссылка на более подробную информацию.
     *
     * @memberOf Cause
     * @property {string}
     */
    link: string;

    /**
     * Родительский следствия.
     *
     * @memberOf Cause
     * @property { Effect[] }
     */
    ancestors: Effect[];

    /**
     * Дочерние следствия.
     *
     * @memberOf Cause
     * @property { Effect[] }
     */
    descendants: Effect[];

    /**
     * Доступные действия над объектом.
     *
     * @memberOf Cause
     * @property { Capability[]}
     */
    capabilities: Capability[];

}
