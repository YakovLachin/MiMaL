import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Cause } from './cause';
import  'rxjs/add/operator/toPromise';
import {toPromise} from "rxjs/operator/toPromise";

@Injectable()
export class CauseService {
    private causeUrl = 'api/cause';
    private headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http) {
    }

    /**
     * Возвращает список "Причин"
     * @todo Переписать на список родительских или дочерних "Следствий" (Effect[])
     * по идентификатору Причины.
     *
     * @returns {Promise<Cause[]> }
     */
    getCauses():Promise<Cause[]> {
        return this.http.get(this.causeUrl, { headers: this.headers })
            .toPromise()
            .then(response => response.json().data as Array<Cause>)
            .catch(this.handleError);
    }
    /**
     * Возвращает  причину по её идентификатору.
     *
     * @param id
     * @returns {Promise<Cause>}
     */
    getCause(id:number):Promise<Cause> {
        const url = `${this.causeUrl}/${id}`;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => response.json().data as Cause)
            .catch(this.handleError);
    }

    /**
     * Обрабатывает возможную ошибку.
     *
     * @param error
     * @returns {any}
     */
    private handleError(error:any):Promise<any> {
        console.error('An error ocurred', error);

        return Promise.reject(error.message || error);
    }

    update(firstCause: Cause):Promise<Cause> {
        const url = `${this.causeUrl}/${firstCause.id}`;
        return this.http
            .post(url, JSON.stringify(firstCause), {headers: this.headers})
            .toPromise()
            .then(() => firstCause)
            .catch(this.handleError);
    }

    create(title: string): Promise<Cause> {
        return this.http
            .post(this.causeUrl, JSON.stringify({title:title}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(cause: Cause): Promise<void> {
        const url = `${this.causeUrl}/${cause.id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
