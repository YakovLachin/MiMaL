<?php

namespace MiMaL\Core\Relation;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Effect\EffectModel;
use MiMaL\Models\DefaultModel;

/**
 * Class Entity
 * @property EffectCauseRelation instance
 * @Entity @Table(name="effect_to_cause_relations")
 */
class EffectCauseRelation extends DefaultModel implements RelationInterface
{
    /**
     * @id @Column(name="effect_id", type="integer", nullable=false)
     */
    protected  $effectId;

    /**
     * @id @Column(name="cause_id", type="integer", nullable=false)
     */
    protected  $causeId;

    /**
     * Следствие родитель.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\Effect\EffectModel", inversedBy="ancestors")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $ancestor;

    /**
     * Причина потомок.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\Cause\CauseModel", inversedBy="descendants")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $descendant;

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "effectId" => 0,
            "causeId"  => 0,
        );
    }

    /**
     * @return string
     */
    public function setDateTimeUpdate()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAncestor()
    {
        return $this->ancestor;
    }

    public function setAncestor($ancestor)
    {
        /**
         * @var EffectModel $ancestor
         */
        $this->effectId = $ancestor->getId();
        $this->ancestor = $ancestor;
    }

    /**
     * @return mixed
     */
    public function getDescendant()
    {
        return $this->descendant;
    }

    public function setDescendant($descendant)
    {
        /**
         * @var CauseModel $descendant
         */
        $this->causeId = $descendant->getId();
        $this->descendant = $descendant;
    }
}