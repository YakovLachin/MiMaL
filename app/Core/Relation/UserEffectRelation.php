<?php

namespace MiMaL\Core\Relation;

use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\User\UserModel;
use MiMaL\Models\DefaultModel;

/**
 * Модель страницы Статьи.
 *
 * Class Entity
 * @property UserCauseRelation instance
 * @Entity @Table(name="user_to_effect_relations")
 */
class UserEffectRelation extends DefaultModel
{

    /**
     * @id @Column(name="user_id", type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @id @Column(name="effect_id", type="integer", nullable=false)
     */
    protected $effectId;

    /**
     * Пользователь - владелец причины.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\User\UserModel", inversedBy="effects")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Следствия прикрепленные к Пользователю.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\Effect\EffectModel", inversedBy="users")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $effect;

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "userId"  => 0,
            "effect_id" => 0,
        );
    }

    /**
     * @return string
     */
    public function setDateTimeUpdate()
    {
        // TODO: Implement setDateTimeUpdate() method.
    }

    /**
     * @return EffectModel
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * @return UserModel
     */
    public function getUser()
    {
        return $this->user;
    }
}
