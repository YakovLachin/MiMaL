<?php

namespace MiMaL\Core\Relation;

use MiMaL\Kernel;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class RelationPimpleProvider  implements ServiceProviderInterface
{

    const RELATION_SERVICE = "relation.service";
    const RELATION_FACTORY = "relation.factory";

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[self::RELATION_FACTORY] = function() {
            return new RelationFactory();
        };

        $pimple[self::RELATION_SERVICE] = function ($c) {
            return new RelationService($c[self::RELATION_FACTORY], $c[Kernel::KERNEL_DATA_BASE_SERVICE]);
        };
    }
}