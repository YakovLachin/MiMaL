<?php

namespace MiMaL\Core\Relation;

interface RelationInterface {
    public function setAncestor($ancestor);
    public function getAncestor();
    public function setDescendant($descendant);
    public function getDescendant();
}