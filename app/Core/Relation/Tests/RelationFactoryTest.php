<?php

namespace MiMaL\Core\Relation\Tests;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\Relation\CauseEffectRelation;
use MiMaL\Core\Relation\EffectCauseRelation;
use MiMaL\Core\Relation\RelationFactory;


class RelationFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function test_GetRelation_ValidParams_ReturnCorrectRelation()
    {
        $factory = new RelationFactory();
        $cause   = new CauseModel();
        $effect  = new EffectModel();

        $this->assertInstanceOf(CauseEffectRelation::class, $factory->getRelation($cause, $effect));
        $this->assertInstanceOf(EffectCauseRelation::class, $factory->getRelation($effect, $cause));

    }
}
