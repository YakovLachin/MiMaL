<?php

namespace MiMaL\Core\Relation;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\User\UserModel;
use MiMaL\Models\DefaultModel;

/**
 * Модель страницы Статьи.
 *
 * Class Entity
 * @property UserCauseRelation instance
 * @Entity @Table(name="user_to_cause_relations")
 */
class UserCauseRelation extends DefaultModel
{

    /**
     * @id @Column(name="user_id", type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @id @Column(name="cause_id", type="integer", nullable=false)
     */
    protected $causeId;

    /**
     * Пользователь - владелец причины.
     *
     * @ManyToOne(targetEntity="\MiMaL\Core\User\UserModel", inversedBy="causes")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * Причины прикрепленные к Пользователю.
     *
     * @ManyToOne(targetEntity="\MiMaL\Core\Cause\CauseModel", inversedBy="users")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $cause;

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "userId"  => 0,
            "causeId" => 0,
        );
    }

    /**
     * @return string
     */
    public function setDateTimeUpdate()
    {
        // TODO: Implement setDateTimeUpdate() method.
    }

    /**
     * @return CauseModel
     */
    public function getCause()
    {
        return $this->cause;
    }

    /**
     * @return UserModel
     */
    public function getUser()
    {
        return $this->user;
    }
}
