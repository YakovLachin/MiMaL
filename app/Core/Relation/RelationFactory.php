<?php

namespace MiMaL\Core\Relation;

class RelationFactory
{
    /**
     * @param $ancestor
     * @param $descendant
     *
     * @return \RelationInterface
     * @throws \Exception
     */
    public function getRelation($ancestor, $descendant) {
        switch (true) {
            case  $ancestor instanceOf \MiMaL\Core\Cause\CauseModel:
                $relations = $this->GetCauseRelations();
                break;
            case  $ancestor instanceOf \MiMaL\Core\Effect\EffectModel:
                $relations = $this->GetEffectRelations();
                break;
            case  $ancestor instanceOf \MiMaL\Core\User\UserModel:
                $relations = $this->GetUserRelations();
                break;
            default:
                throw new \Exception("Объект" . get_class($ancestor) . "Не может быть родительским");
        }
        $classOf = get_class($descendant);
        $relation = $relations[$classOf];

        return $relation;
    }


    /**
     * При условии что Cause является родительким элементом.
     * Возвращает связь между Сause и переданым эелемнетом.
     */
    protected function GetCauseRelations() {
        return array(
            \MiMaL\Core\Effect\EffectModel::class => new \MiMaL\Core\Relation\CauseEffectRelation(),
        );
    }

    /**
     * При условии что Effect является родительким элементом.
     * Возвращает связь между Effect и переданым эелемнетом.
     */
    protected function GetEffectRelations() {
        return array(
            \MiMaL\Core\Cause\CauseModel::class => new \MiMaL\Core\Relation\EffectCauseRelation(),
        );
    }

    /**
     * При условии что Effect является родительким элементом.
     * Возвращает связь между Effect и переданым эелемнетом.
     */
    protected function GetUserRelations() {
        return array(
            \MiMaL\Core\Cause\CauseModel::class   => new \MiMaL\Core\Relation\UserCauseRelation(),
            \MiMaL\Core\Effect\EffectModel::class => new \MiMaL\Core\Relation\UserEffectRelation(),
        );
    }
}