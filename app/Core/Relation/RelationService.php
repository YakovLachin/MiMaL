<?php

namespace MiMaL\Core\Relation;

use MiMaL\Services\DataBase;

class RelationService
{

    protected $ancestor;

    protected $descendant;

    protected $relationFactory;
    /**
     * @var DataBase
     */
    private $dataBase;

    /**
     * RelationService constructor.
     * @param RelationFactory $relationFactory
     * @param DataBase $dataBase
     */
    public function __construct(RelationFactory $relationFactory, DataBase $dataBase)
    {
        $this->relationFactory = $relationFactory;
        $this->dataBase = $dataBase;
    }

    /**
     * @param mixed $ancestor
     *
     * @return $this
     */
    public function setAncestor($ancestor)
    {
        $this->ancestor = $ancestor;

        return $this;
    }

    /**
     * @param mixed $descendant
     *
     * @return $this
     */
    public function setDescendant($descendant)
    {
        $this->descendant = $descendant;

        return $this;
    }

    public function relate() {
        /**
         * @var RelationInterface $relation
         */
        $relation = $this->relationFactory->getRelation($this->ancestor, $this->descendant);

        $relation->setAncestor($this->ancestor);
        $relation->setDescendant($this->descendant);

        $this->dataBase->writeEntity($relation);
    }
}