<?php

namespace MiMaL\Core\Relation;

use MiMaL\Models\DefaultModel;

/**
 * Class Entity
 * @property CauseEffectRelation instance
 * @Entity @Table(name="cause_to_effect_relations")
 */
class CauseEffectRelation extends DefaultModel implements RelationInterface
{

    /**
     * @id @Column(name="cause_id", type="integer", nullable=false)
     */
    protected  $causeId;
    /**
     * @id @Column(name="effect_id", type="integer", nullable=false)
     */
    protected  $effectId;

    /**
     * Причина Родитель.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\Cause\CauseModel", inversedBy="ancestors")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $ancestor;

    /**
     * Следствие потомок.
     *
     * @ManyToOne(targetEntity="MiMaL\Core\Effect\EffectModel", inversedBy="descendants")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $descendant;

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "causeId"  => 0,
            "effectId" => 0,
        );
    }

    /**
     * @return string
     */
    public function setDateTimeUpdate()
    {
        return $this;
    }

    /**
     * @return \MiMaL\Core\Cause\CauseModel
     */
    public function getAncestor()
    {
        return $this->ancestor;
    }

    /**
     * @return \MiMaL\Core\Effect\EffectModel
     */
    public function getDescendant()
    {
        return $this->descendant;
    }

    public function setAncestor($ancestor)
    {
        /**
         * @var \MiMaL\Core\Cause\CauseModel $ancestor
         */
        $this->causeId  = $ancestor->getId();
        $this->ancestor = $ancestor;
    }

    public function setDescendant($descendant)
    {
        /**
         * @var \MiMaL\Core\Effect\EffectModel $descendant
         */
        $this->effectId   = $descendant->getId();
        $this->descendant = $descendant;
    }
}