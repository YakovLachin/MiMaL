<?php

namespace MiMaL\Core\Security;

use MiMaL\Core\User\UserModel;
use MiMaL\Core\User\UserService;
use Firebase\JWT\JWT;
use MiMaL\Encryption\Crypter;
use MiMaL\Errors\Users\AuthorizeError;


class SecurityService {

    protected static $instance;

    /**
     * Секретный ключ для шифровки расшифровки токена
     * @var string
     */
    private $secretKey;

    /**
     * Сервис сайта.
     * @var Site
     */
    private $serviceSite;

    /**
     * Сервис пользователей.
     * @var UserService
     */
    private $serviceUsers;
    /**
     * @var Crypter
     */
    private $crypter;

    /**
     * @param string      $secretKey    Секретный ключ для шифровкии расшифровки токена.
     * @param Site        $serviceSite  Сервис сайта.
     * @param UserService $serviceUsers Сервис пользователей.
     * @param Crypter     $crypter
     */
    public function __construct($secretKey, $serviceSite, $serviceUsers, $crypter)
    {
        $this->secretKey    = $secretKey;
        $this->serviceSite  = $serviceSite;
        $this->serviceUsers = $serviceUsers;
        $this->crypter      = $crypter;
    }

    /**
     * Возвращает токен по входящим данным.
     *
     * @param AuthObjectModel $data
     * @return string
     *
     * Не используется.
     */
    public function getToken(AuthObjectModel $data)
    {
        $date = new \DateTime('now');
        $expired = $date->modify('+1 day');
        $token = array(
            "exp"  => $expired->getTimestamp(),
            "data" => $data
        );


        $jwt = JWT::encode($token, $this->secretKey);

        return $jwt;
    }

    /**
     * Возвращает данные из токена.
     *
     * @param $jwt
     * @return mixed[]
     */
    protected function dataFromjwt($jwt) {
        $data = JWT::decode($jwt, $this->secretKey, array('HS256'));
        return $data->data;
    }

    /**
     * Аунтифицирует по токену.
     *
     * @param $token
     * @return bool
     * @throws \MiMal\Errors\Users\AuntificationError
     */
    public function auntificateByToken($token) {

        $authObject = new AuthObjectModel();
        $authObject->setToken($token);
        $this->crypter->decrypt($authObject);

        $data  = get_object_vars($authObject->getDecryptedData());
        $condition = array(
            "id" => $data["id"]
        );

        /**
         * @var \MiMaL\Core\User\UserModel $user
         */
        $user = $this->serviceUsers->userByCondition($condition);

        if ($user) {
            $this->serviceSite->setUser($user);
        } else {
            throw new \MiMal\Errors\Users\AuntificationError("User not Found");
        }

        $this->crypter->encrypt($authObject);

        $this->serviceSite->setUserToken($authObject->getToken());

        return true;
    }

    /**
     * Авторизирует польователя по email и паролю.
     *
     * @param $email
     * @param $password
     *
     * @return UserModel
     *
     * @throws AuthorizeError
     */
    public function authorize($email, $password)
    {
        $condition = array(
            "email" => $email
        );

        /**
         * @var UserModel $user
         */
        $user = $this->serviceUsers->UserByCondition($condition);
        if ($user && $user->checkPassword($password)) {
            $this->setCurrentUser($user);
        } else {
            throw new AuthorizeError("Логин или пароль - не действительны.");
        }
    }

    public function registrate($email, $password) {
        /**
         * @var UserModel $user
         */
        $user = $this->serviceUsers->createUser($email, $password);
        if ($user) {
            $this->setCurrentUser($user);
        } else {
            throw new AuthorizeError("Ошибка при регистрации.");
        }
    }

    /**
     * @param UserModel $user
     */
    protected function setCurrentUser(UserModel $user) {
        $this->serviceSite->setUser($user);
        $authObject = new AuthObjectModel();
        $authObject->setDecryptedData(
            array(
                "id" => $user->getId(),
            )
        );

        $this->crypter->encrypt($authObject);

        $this->serviceSite->setUserToken($authObject->getToken());
    }
}
