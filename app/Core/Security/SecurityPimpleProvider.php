<?php

namespace MiMaL\Core\Security;

use MiMaL\Core\User\UserPimpleProvider;
use MiMaL\Kernel;
use MiMaL\Services\SecurityService;
use Pimple\ServiceProviderInterface;
use Pimple\Container;

class SecurityPimpleProvider implements ServiceProviderInterface
{

    const SECURITY_SERVICE = 'security.service';

    const SECURITY_SECRET_KEY = 'security.secret.key';

    public function register(Container $pimple)
    {
        /**
         * @param Container $pimple
         *
         * @return SecurityService
         */
        $pimple[self::SECURITY_SERVICE] = function($pimple) {
            $secretKey   = $pimple->offsetGet(self::SECURITY_SECRET_KEY);
            $serviceSite = $pimple->offsetGet(Kernel::KERNEL_SITE);
            $serviceUser = $pimple->offsetGet(UserPimpleProvider::USER_SERVICE);
            $crypter     = $pimple->offsetGet(Kernel::CRYPTER);

            return new SecurityService($secretKey, $serviceSite, $serviceUser, $crypter);
        };

        /**
         * @param Container $pimple
         *
         * @return string
         */
        $pimple[self::SECURITY_SECRET_KEY] = function ($pimple) {
            try {
                $config = $pimple->offsetGet("config");
                $secretKey = $config["serviceSecurity"]["secretKey"];
            } catch (\Exception $exception) {
                $secretKey = "SECRET_FROM_CONFIG_NOT_FOUND_WARNING!!DASDASDASDASD213d";
            };

            return $secretKey;
        };
    }
}
