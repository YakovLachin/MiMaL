<?php

namespace MiMaL\Core\Security;

use MiMaL\Encryption\CryptedInterface;

class AuthObjectModel implements CryptedInterface
{
    private $token;
    private $salt;
    private $decryptedData;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDecryptedData()
    {
        return $this->decryptedData;
    }

    /**
     * @param mixed $decryptedData
     */
    public function setDecryptedData($decryptedData)
    {
        $this->decryptedData = $decryptedData;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param  $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }
}
