<?php

namespace MiMaL\Core\Cause;

use MiMaL\Core\Cause\Presenter\CapabilitiesPresenter;
use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\Effect\EffectPimpleProvider;
use MiMaL\Core\Effect\EffectPresenter;
use Pimple\Container;

class CausePresenter
{
    /**
     * @var CauseModel
     */
    protected $cause;

    /**
     * @var bool
     */
    protected $withAncestors;

    /**
     * @var bool
     */
    protected $withDescendants;

    /**
     * @var bool
     */
    protected $withCapabilities;

    /**
     * CausePresenter constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return CauseViewModel|null
     */
    public function getView()
    {

        $cause = $this->cause;

        if ($cause->isDeleted()) {
            return null;
        }

        $result = new CauseViewModel();

        $result->setId($cause->getId());
        $result->setTitle($cause->getTitle());
        $result->setContent($cause->getContent());
        $result->setLink($cause->getLink());
        $result->setPreviewLink($cause->getPreviewLink());
        $result->setDateCreated($cause->getDateCreated());

        if ($this->withCapabilities) {
            /**
             * @var CapabilitiesPresenter $capabilitiesPresenter
             */
            $capabilitiesPresenter = $this->container->offsetGet(CausePimpleProvider::CAUSE_CAPABILITIES_PRESENTER);
            $capabilities = $capabilitiesPresenter->setCause($this->cause)->getView();
            $result->setCapabilities($capabilities);
        }

        if ($this->withAncestors) {
            $this->addAncestors($result);
        }

        if ($this->withDescendants) {
            $this->addDescedants($result);
        }

        return $result;
    }

    /**
     * @param CauseModel $cause
     * @return $this
     */
    public function setCause($cause)
    {
        $this->cause = $cause;

        return $this;
    }

    /**
     * @param bool $withAncestors
     * @return $this
     */
    public function withAncestors($withAncestors)
    {
        $this->withAncestors = $withAncestors;

        return $this;
    }

    private function addAncestors(CauseViewModel $causeView)
    {
        /**
         * @var EffectPresenter $effectPresenter
         */
        $effectPresenter = $this->container->offsetGet(EffectPimpleProvider::EFFECT_PRESENTER);
        $ancestors = $this->cause->getAncestors();

        $views = array();

        /**
         * @var EffectModel[] $ancestors
         */
        foreach ($ancestors as $ancestor) {
            $ancestorView = $effectPresenter->setEffect($ancestor)->getView();
            $views []= $ancestorView;
        }

        $causeView->setAncestors($views);
    }


    /**
     * @param bool $withDescendants
     *
     * @return $this
     */
    public function withDescendants($withDescendants)
    {
        $this->withDescendants = $withDescendants;

        return $this;
    }

    private function addDescedants(CauseViewModel $causeView)
    {
        /**
         * @var EffectPresenter $effectPresenter
         */
        $effectPresenter = $this->container->offsetGet(EffectPimpleProvider::EFFECT_PRESENTER);
        $descedants = $this->cause->getDescendats();
        $views = array();

        /**
         * @var EffectModel[] $ancestors
         */
        foreach ($descedants as $descedant) {
            $descedantView = $effectPresenter->setEffect($descedant)->getView();
            $views []= $descedantView;
        }

        $causeView->setDescendants($views);
    }

    /**
     * @param bool $withCapabilities
     *
     * @return $this
     */
    public function withCapabilities($withCapabilities)
    {
        $this->withCapabilities = $withCapabilities;

        return $this;
    }
}
