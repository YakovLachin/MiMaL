<?php

namespace MiMaL\Core\Cause;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class CauseSilexProvider implements ControllerProviderInterface
{
    /**
     * Returns routes to connect to the given application.
     *
     * @param \Silex\Application $app An Application instance
     *
     * @return \Silex\ControllerCollection A ControllerCollection instance
     */
    public function connect(\Silex\Application $app)
    {
        /**
         * @var \Pimple\Container $container
         */
        $container = $app;

        $container->register(new CausePimpleProvider());

        /**
         * @var \Silex\ControllerCollection $causes
         */
        $causes = $app['controllers_factory'];

        $causes->get('{id}', function (Request $request) use ($container, $app) {
            $id      = $request->get('id');
            /**
             * @var CauseService $service
             */
            $service = $container['cause.service'];
            $cause   = $service->readById($id);
            /**
             * @var \MiMaL\Core\Cause\CausePresenter $presenter
             */
            $presenter = $container->offsetGet(CausePimpleProvider::CAUSE_PRESENTER);
            $modelView = $presenter->setCause($cause)
                ->withAncestors(true)
                ->withDescendants(true)
                ->withCapabilities(true)
                ->getView();

            return $app->json(array("data" => $modelView->getValues()));
        });

        $causes->get('{id}/descendants', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var CauseService $service
             */
            $service     = $container['cause.service'];
            $descendants = $service->readDescendantsById($id);

            $result    = array();

            foreach ($descendants as $descendant) {
                $result[] = $descendant->getValues();
            }

            return $app->json($result);
        });

        $causes->get('{id}/ancestors', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var CauseService $service
             */
            $service   = $container['cause.service'];
            $ancestors = $service->readAncestorsById($id);
            $result    = array();

            foreach ($ancestors as $ancestor) {
                $result[] = $ancestor->getValues();
            }

            return $app->json($result);
        });

        $causes->post('/{parentType}/{parentId}', function (Request $request) use ($container, $app) {
            $parentId   = $request->get("parentId");
            $parentType = $request->get("parentType");
            $data  = json_decode($request->getContent(), true);

            /**
             * @var CauseService $service
             */
            $service = $container['cause.service'];
            $cause   = $service->create($data, $parentType, $parentId);

            return $app->json($cause->getValues());
        });

        $causes->post('{id}', function (Request $request) use ($container, $app) {
            $id    = $request->get("id");
            $data  = json_decode($request->getContent(), true);

            /**
             * @var CauseService $service
             */
            $service = $container['cause.service'];
            $cause   = $service->update($id, $data);

            return $app->json($cause->getValues());
        });

        $causes->delete('{id}', function() use ($container, $app) {

        });
        return $causes;
    }
}
