<?php

namespace MiMaL\Core\Cause\Presenter;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\SecureLink\LinkSecuringInterface;
use MiMaL\Specification\SpecificationInterface;

class CapabilitiesPresenter
{
    /**
     * @var SpecificationInterface
     */
    private $ownerSpecificator;

    /**
     * @var CauseModel
     */
    protected $cause;
    /**
     * @var LinkSecuringInterface
     */
    private $linkSecuring;


    /**
     * CapabilitiesPresenter constructor.
     * @param SpecificationInterface $ownerSpecificator
     * @param LinkSecuringInterface $linkSecuring
     */
    public function __construct(
        SpecificationInterface $ownerSpecificator,
        LinkSecuringInterface $linkSecuring
    )
    {
        $this->ownerSpecificator = $ownerSpecificator;
        $this->linkSecuring = $linkSecuring;
    }

    public function getView()
    {
        $capabilities = array();
        if ($this->ownerSpecificator->isSatisfiedBy($this->cause)) {
            $capabilities[] = array(
                "rel"    => "update",
                "method" => "POST",
                "link"   => $this->linkSecuring->generateSecureLink('/api/cause/' . $this->cause->getId())
            );

            $capabilities[] = array(
                "rel"    => "create",
                "method" => "POST",
                "link"   => $this->linkSecuring->generateSecureLink('/api/cause/create'),
            );
        }

        return $capabilities;
    }

    /**
     * @param CauseModel $cause
     * @return $this
     */
    public function setCause($cause)
    {
        $this->cause = $cause;

        return $this;
    }
}