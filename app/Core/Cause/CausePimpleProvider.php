<?php

namespace MiMaL\Core\Cause;

use MiMaL\Core\Cause\Specificators\VisitorIsOwnerOfCause;
use MiMaL\Core\Relation\RelationPimpleProvider;
use MiMaL\Core\SecureLink\SecureLinkPimpleProvider;
use MiMaL\Kernel;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CausePimpleProvider implements ServiceProviderInterface
{
    /**
     * @see CauseService
     */
    const CAUSE_SERVICE = "cause.service";

    /**
     * @see CausePresenter
     */
    const CAUSE_PRESENTER = "cause.presenter";

    /**
     * @see VisitorIsOwnerOfCause
     */
    const CAUSE_SPECIFICATOR_VISITOR_IS_OWNER = "cause.specificator.visitor_is_owner_of_cause";
    const CAUSE_CAPABILITIES_PRESENTER        = "cause.presenter.capabilities_presenter";

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param \Pimple\Container $pimple A container instance
     */
    public function register(\Pimple\Container $pimple)
    {
        $pimple[self::CAUSE_SERVICE] = function($pimple) {
            $dataBaseService = $pimple[\MiMaL\Kernel::KERNEL_DATA_BASE_SERVICE];
            $relationService = $pimple[RelationPimpleProvider::RELATION_SERVICE];
            return new CauseService($dataBaseService, $relationService);
        };

        /**
         * @param \Pimple\Container $pimple
         *
         * @return CausePresenter
         */
        $pimple[self::CAUSE_PRESENTER] = $pimple->factory(function ($pimple) {
            return new CausePresenter($pimple);
        });

        $pimple[self::CAUSE_CAPABILITIES_PRESENTER] = $pimple->factory(function ($pimple) {

            /**
             * @var Container $pimple
             */
            $ownerSpecificator = $pimple->offsetGet(self::CAUSE_SPECIFICATOR_VISITOR_IS_OWNER);
            $secureLinkService = $pimple->offsetGet(SecureLinkPimpleProvider::SECURE_LINK);

            return new \MiMaL\Core\Cause\Presenter\CapabilitiesPresenter($ownerSpecificator, $secureLinkService);
        });

        /**
         * @param \Pimple\Container $pimple
         *
         * @return VisitorIsOwnerOfCause
         */
        $pimple[self::CAUSE_SPECIFICATOR_VISITOR_IS_OWNER] = function ($pimple) {
            /**
             * @var \MiMaL\Services\Site $site
             */
            $site = $pimple->offsetGet(Kernel::KERNEL_SITE);

            return new VisitorIsOwnerOfCause($site);
        };
    }
}
