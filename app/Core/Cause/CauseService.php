<?php

namespace MiMaL\Core\Cause;

use MiMaL\Core\Relation\RelationService;
use MiMaL\Services\DataBase;
use MiMaL\Models\Types;

/**
 * Менеджер для страниц
 * Class Entities
 *
 * @package MiMaL\managers
 */
class CauseService
{
    protected static $instance;

    /**
     * @var DataBase $dataBaseService
     */
    protected $dataBaseService;
    /**
     * @var RelationService
     */
    private $relationService;

    /**
     * @param DataBase $dataBaseService Сервис для работы с БД.
     * @param RelationService $relationService
     */
    public function __construct(DataBase $dataBaseService, RelationService $relationService)
    {
        $this->dataBaseService = $dataBaseService;
        $this->relationService = $relationService;
    }


    /**
     * Возвращает модель сущности по идентификатору сущности.
     *
     * @param int $id Идентификатор сущности.
     *
     * @return CauseModel
     */
    public function readById($id)
    {
        /**
         * @var DataBase $service
         */
        $service    = $this->dataBaseService;
        $conditions = array(
            "id"         => $id,
        );
        $cause = $service->readEntity(Types::CAUSE, $conditions);

        return $cause;
    }

    /**
     * Удаляет модель сущности по идентификатору сущности.
     *
     * @param int $id Идентификатор сущности.
     *
     * @return mixed
     */
    public function deleteById($id)
    {
        /**
         * @var CauseModel $cause
         */
        $cause = $this->readById($id);
        try {
            $deletedEntity = $cause->rewriteModel(
                array(
                    "isDeleted" => true
                )
            );

            return $deletedEntity;
        } catch(\Exception $exception) {

            return $exception->getMessage();
        }
    }

    /**
     * Создает новую сущность исходя из переданных полей.
     *
     * @param $fields
     * @return mixed
     */
    public function create($fields, $parentType, $parentId)
    {
        $cause     = new CauseModel($fields);
        $cause->setDateTimeUpdate();
        $service = $this->dataBaseService;

        $parent = $service->readEntity($parentType, array(
            "id" => $parentId
        ));


        $result  = $service->writeEntity($cause);

        $this->relationService
            ->setAncestor($parent)
            ->setDescendant($result)
            ->relate();

        return $result;
    }

    /**
     * Обновляет сущность.
     *
     * @param int $id           Идентификатор сущности.
     * @param mixed[] $fields   Массив полей, которые будут перезаписаны.
     *
     * @return mixed
     */
    public function update($id, $fields)
    {
        /**
         * @var CauseModel $cause
         */
        $cause = $this->readById($id);
        $cause->rewriteModel($fields);
        $cause->setDateTimeUpdate();

        /**
         * @var DataBase $service
         */
        $service = $this->dataBaseService;
        /**
         * @var CauseModel $updatedCause
         */
        $updatedCause = $service->writeEntity($cause);

        return $updatedCause;
    }

    public function readAncestorsById($id)
    {
        $cause  = $this->readById($id);

        /**
         * @var \Doctrine\ORM\PersistentCollection $relations
         */
        $collection = $cause->getAncestorRelationsCollection();
        $relations  = $collection->getValues();
        $ancestors  = array();
        /**
         * @var \MiMal\Core\Relation\EffectCauseRelation $relation
         */
        foreach ($relations as $relation) {
           $ancestors[]= $relation->getAncestor();
        }

        return $ancestors;
    }

    public function readDescendantsById($id)
    {
        $cause  = $this->readById($id);
        /**
         * @var \Doctrine\ORM\PersistentCollection $descendants
         */
        $collection  = $cause->getDescendantRelationsCollection();
        $relations   = $collection->getValues();
        $descendants = array();

        /**
         * @var \MiMal\Core\Relation\CauseEffectRelation $relation
         */
        foreach ($relations as $relation) {
            $descendants[]= $relation->getDescendant();
        }

        return $descendants;
    }
}
