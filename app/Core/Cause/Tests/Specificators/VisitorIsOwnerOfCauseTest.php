<?php

namespace MiMaL\Core\Cause\Tests\Specificators;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Cause\Specificators\VisitorIsOwnerOfCause;
use MiMaL\Core\User\UserModel;
use MiMaL\Services\Site;
use PHPUnit_Framework_TestCase;

class VisitorIsOwnerOfCauseTest extends PHPUnit_Framework_TestCase
{
    public function test_isSatisfiedBy_UserIsOwner_ReturnTrue()
    {
        $visitor = new UserModel();
        $visitor->setId(1);
        $owner  = new UserModel();
        $owner->setId(1);
        $owners = array($owner);

        /**
         * @var Site|\PHPUnit_Framework_MockObject_MockObject $site
         */
        $site = $this->createMock(Site::class);
        $site->method($this->anything())->willReturn($visitor);
        $specificator = new VisitorIsOwnerOfCause($site);
        $cause = $this->createMock(CauseModel::class);
        $cause->method('getUsers')->willReturn($owners);

        $this->assertTrue($specificator->isSatisfiedBy($cause));
    }

    public function test_isSatisfiedBy_UserIsNotOwner_ReturnFalse()
    {
        $visitor = new UserModel();
        $visitor->setId(1);
        $owner  = new UserModel();
        $owner->setId(2);
        $owners = array($owner);

        /**
         * @var Site|\PHPUnit_Framework_MockObject_MockObject $site
         */
        $site = $this->createMock(Site::class);
        $site->method($this->anything())->willReturn($visitor);
        $specificator = new VisitorIsOwnerOfCause($site);
        $cause = $this->createMock(CauseModel::class);
        $cause->method('getUsers')->willReturn($owners);

        $this->assertFalse($specificator->isSatisfiedBy($cause));
    }
}
