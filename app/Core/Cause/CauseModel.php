<?php

namespace MiMaL\Core\Cause;

use MiMaL\Core\Relation\UserCauseRelation;
use MiMaL\Core\User\UserModel;
use MiMaL\Models\DefaultModel;

/**
 * Модель страницы Причины.
 *
 * @property CauseModel instance
 * @Entity @Table(name="causes")
 */
class CauseModel extends DefaultModel
{
    /**
     * int Идентификатор причины.
     * @id @Column(type="integer", name="cause_id") @GeneratedValue
     */
    protected $id;

    /**
     * @var bool Флаг - удалена ли сущность или нет.
     *
     * @Column (type="boolean", name="is_deleted")
     */
    protected $isDeleted;

    /**
     * string Заголовок причнины.
     * @Column(type="string")
     */
    protected $title;

    /**
     * string  Содержимое сущности.
     * @Column(type="text")
     */
    protected $content;

    /**
     * Ссылка на сущность.
     * @Column(type="string", nullable=true)
     */
    protected $link;

    /**
     * Ссылка на превью сущности.
     * @Column(type="string",name="preview_link", nullable=true)
     */
    protected $previewLink;

    /**
     * @var string Дата создания сушности.
     * @Column(type="integer", name="date_created", nullable=true)
     */
    protected $dateCreated;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\EffectCauseRelation", mappedBy="descendant")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $ancestors;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\CauseEffectRelation", mappedBy="ancestor")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $descendants;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @var @OneToMany(targetEntity="\MiMaL\Core\Relation\UserCauseRelation", mappedBy="cause")
     * @JoinColumn(name="cause_id", referencedColumnName="cause_id")
     */
    protected $users;

    /**
     * Возвращает
     * Массив полей Модели Entity.
     *
     * @return array
     */
    final public function fields()
    {
        return array(
            "id"          => 0,
            "isDeleted"   => false,
            "title"       => '',
            "content"     => '',
            "link"        => '',
            "previewLink" => '',
            "dateCreated" => 0,
        );
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDateTimeUpdate()
    {
        $date = new \DateTime('now');
//        $this->date =$date->format("Y-m-d H:i:s");
//        $this->date = $date;
    }

    /**
     * Возвращает дату последних изменений.
     *
     * @return string
     */
    public function getDate()
    {
        return $this->dateCreated;
    }

    /**
     * Возвращает коллекцию связей с предками данной причины.
     *
     * @return mixed
     */
    public function getAncestorRelationsCollection()
    {
        return $this->ancestors;
    }


    /**
     * @return array
     */
    public function getAncestors()
    {
        $relations = $this->ancestors->getValues();

        $ancestors = array();

        /**
         *@var \MiMaL\Core\Relation\CauseEffectRelation[] $relations
         */
        foreach ($relations as $relation) {
            $ancestors[] = $relation->getAncestor();
        }

        return $ancestors;
    }

    /**
     * Возвращает коллекцию связей с потомками данной причины.
     *
     * @return mixed
     */
    public function getDescendantRelationsCollection()
    {
        return $this->descendants;
    }

    /**
     * Возвращает коллекцию связей с пользователем данной причины.
     *
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getUserRelationsCollection()
    {
        return $this->users;
    }

    /**
     * @return UserModel[]
     */
    public function getUsers() {
        $relations = $this->users->getValues();
        $users = array();

        /**
         * @var UserCauseRelation[] $relations
         */
        foreach ($relations as $relation) {
            $users []= $relation->getUser();
        }

        return $users;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getPreviewLink()
    {
        return $this->previewLink;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function getDescendats()
    {
        $relations = $this->descendants->getValues();

        $descendants = array();

        /**
         *@var \MiMaL\Core\Relation\CauseEffectRelation[] $relations
         */
        foreach ($relations as $relation) {
            $descendants[] = $relation->getDescendant();
        }

        return $descendants;
    }
}
