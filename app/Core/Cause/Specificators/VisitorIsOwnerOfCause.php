<?php

namespace MiMaL\Core\Cause\Specificators;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Relation\UserCauseRelation;
use MiMaL\Core\User\UserModel;
use MiMaL\Services\Site;
use MiMaL\Specification\CompositeSpecification;

class VisitorIsOwnerOfCause extends CompositeSpecification
{
    /**
     * @var Site
     */
    private $site;

    /**
     * VisitorIsOwnerOfCause constructor.
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    public function isSatisfiedBy($cause)
    {
        /**
         * @var CauseModel $cause
         * @var UserModel[] $users
         */
        $users = $cause->getUsers();
        $visitor = $this->site->getUser();
        if (null == $visitor) {
            return false;
        }

        foreach ($users as $user) {
            if ($user->getId() == $visitor->getId()) {
                return true;
            }
        }

        return false;
    }
}
