<?php

namespace MiMaL\Core\User;

use MiMaL\Core\Cause\CausePimpleProvider;
use MiMaL\Core\SecureLink\SecureLinkPimpleProvider;
use MiMaL\Services\DataBase;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class UserPimpleProvider implements ServiceProviderInterface
{

    /**
     * @see UserService
     */
    const USER_SERVICE                      = "user.service";

    /**
     * @see UserPresenter
     */
    const USER_PRESENTER                    = "user.presenter";

    /**
     * @see Specificators\UserIsVisitor
     */
    const USER_SPECIFICATOR_USER_IS_VISITOR = "user.specificators.user_is_visitor";

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[self::USER_SERVICE] = function ($pimple) {

            /**
             * @var DataBase $service
             */
            $service = $pimple[\MiMaL\Kernel::KERNEL_DATA_BASE_SERVICE];

            return new UserService($service);
        };


        $pimple[self::USER_PRESENTER] = function($pimple) {
            /**
             * @var \Pimple\Container $pimple
             * @var \MiMaL\Core\Cause\CausePresenter $causePresenter
             */
            $causePresenter = $pimple->offsetGet(CausePimpleProvider::CAUSE_PRESENTER);

            /**
             * @var \MiMaL\Specification\SpecificationInterface
             */
            $editSpecificator  = $pimple->offsetGet(self::USER_SPECIFICATOR_USER_IS_VISITOR);

            /**
             * @var \MiMaL\Core\SecureLink\LinkSecuringInterface $linkSecuringInterface
             */
            $linkSecuringInterface = $pimple->offsetGet(SecureLinkPimpleProvider::SECURE_LINK);

            return new UserPresenter($causePresenter, $editSpecificator, $linkSecuringInterface);
        };

        $pimple[self::USER_SPECIFICATOR_USER_IS_VISITOR] = function ($pimple)
        {
            $siteService = $pimple[\MiMaL\Kernel::KERNEL_SITE];

            return new Specificators\UserIsVisitor($siteService);
        };
    }
}
