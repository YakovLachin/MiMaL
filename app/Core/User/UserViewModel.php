<?php

namespace MiMaL\Core\User;

use MiMaL\Models\DefaultModel;

/**
 * Модель данных ответа клиенту
 */
class UserViewModel extends DefaultModel
{
    public $id;

    public $email;

    public $causes;

    public $links;

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "id"     => 0,
            "email"  => "",
            "causes" => array(),
            "links"  => array(),
        );
    }

    public function toArray()
    {
        return (array)$this;
    }

    /**
     * @return string
     */
    public function setDateTimeUpdate()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCauses()
    {
        return $this->causes;
    }

    /**
     * @param mixed $causes
     */
    public function setCauses($causes)
    {
        $this->causes = $causes;
    }

    /**
     * @return mixed
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param mixed $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }
}