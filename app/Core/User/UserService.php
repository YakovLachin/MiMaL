<?php

namespace MiMaL\Core\User;

use MiMaL\Services\DataBase;
use MiMaL\Models\Types;

class UserService
{
    /**
     * @var DataBase
     */
    private $dataBaseService;

    /**
     * Password constructor.
     * @param DataBase $dataBaseService
     */
    public function __construct(DataBase $dataBaseService)
    {
        $this->dataBaseService = $dataBaseService;
    }

    /**
     * @return UserModel
     */
    public function userByCondition($conditions)
    {
        /**
         * @var DataBase $service
         */
        $service = $this->dataBaseService;

        /**
         * @var UserModel $user
         */
        $user   = $service->readEntity(Types::USER, $conditions);

        return $user;
    }

    /**
     * @param $id
     * @param $fields
     *
     * @return UserModel
     */
    public function updateUser($id, $fields)
    {
        $condition = array(
            "id" => $id
        );
        $user = $this->userByCondition($condition);
        $user->rewriteModel($fields);
        /**
         * @var DataBase $service
         */
        $service = $this->dataBaseService;
        /**
         * @var UserModel $updatedEntity
         */
        $updatedEntity = $service->writeEntity($user);

        return $updatedEntity;
    }

    /**
     * @param $email
     * @param $password
     *
     * @return UserModel
     *
     * @throws \Exception
     */
    public function createUser($email, $password)
    {
        $condition = array("email" => $email);
        if ($this->userByCondition($condition)) {
            throw new \Exception('User Exist');
        }

        /**
         * @var UserModel( $user
         */
        $user = new UserModel($condition);
        $user->setPassword($password);
        $user->setDateTimeUpdate();
        $user = $this->dataBaseService->writeEntity($user);

        return $user;
    }

}
