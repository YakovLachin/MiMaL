<?php

namespace MiMaL\Core\User\Specificators;

use MiMaL\Core\User\UserModel;
use MiMaL\Specification\CompositeSpecification;

class UserIsVisitor extends CompositeSpecification
{
    protected $visitor;
    /**
     * @var \MiMaL\Services\Site
     */
    private $siteService;

    /**
     * UserIsVisitor constructor.
     *
     * @param \MiMaL\Services\Site $siteService
     */
    public function __construct(\MiMaL\Services\Site $siteService)
    {
        $this->siteService = $siteService;
    }

    /**
     * @param UserModel $user
     *
     * @return bool
     */
    public function isSatisfiedBy($user)
    {
        $visitor = $this->siteService->getUser();

        if (null === $visitor) {
            return false;
        }

        return $user->getId() === $visitor->getId();
    }
}
