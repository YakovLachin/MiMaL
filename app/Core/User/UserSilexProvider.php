<?php

namespace MiMaL\Core\User;

use MiMaL\Core\Relation\UserCauseRelation;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class UserSilexProvider implements ControllerProviderInterface
{
    /**
     * Returns routes to connect to the given application.
     *
     * @param \Silex\Application $app An Application instance
     *
     * @return \Silex\ControllerCollection A ControllerCollection instance
     */
    public function connect(\Silex\Application $app)
    {
        /**
         * @var \Pimple\Container $container
         */
        $container = $app;

        $container->register(new UserPimpleProvider());

        /**
         * @var \Silex\ControllerCollection $user
         */
        $user = $app['controllers_factory'];

        $user->get('{id}', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var UserService $service
             */
            $service = $container[UserPimpleProvider::USER_SERVICE];

            $user    = $service->userByCondition(array('id' => $id));

            /**
             * @var UserPresenter $presenter
             */
            $presenter = $container->offsetGet(UserPimpleProvider::USER_PRESENTER);

            $result    = $presenter->setUser($user)->setWithCauses(true)->getView();

            return $app->json(array("data" => $result->getValues()));
        });

        $user->get('{id}/parent-causes', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');

            /**
             * @var UserService $service
             */
            $service = $container[UserPimpleProvider::USER_SERVICE];

            $user    = $service->userByCondition(array('id' => $id));

            /**
             * @var UserPresenter $presenter
             */
            $presenter = $container->offsetGet(UserPimpleProvider::USER_PRESENTER);

            $result    = $presenter->setUser($user)->setWithCauses(true)->getView();

            return $app->json(array("data" => $result->getValues()));
        });

//        $causes->post('', function (Request $request) use ($container, $app) {
//            $data  = $request->request->all();
//
//            /**
//             * @var CauseService $service
//             */
//            $service = $container['cause.service'];
//            $cause   = $service->create($data);
//
//            return $app->json($cause->getValues());
//        });

        return $user;
    }
}
