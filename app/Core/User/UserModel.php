<?php

namespace MiMaL\Core\User;

use MiMaL\Core\Relation\UserCauseRelation;
use MiMaL\Models\DefaultModel;

/**
 * Модель Пользователя приложения.
 *
 * @property UserModel instance
 * @Entity @Table(name="users")
 */
class UserModel extends DefaultModel
{
    /**
     * Стандартный тип пользователя.
     */
    const TYPE_DEFAULT = 0;

    /**
     * Премиум тип пользователя.
     */
    const TYPE_PREMIUM = 1;

    /**
     * Тип пользователя - администратор.
     */
    const TYPE_ADMIN   = 2;

    /**
     * Идентификатор пользователя.
     * @id @Column(type="integer", name="user_id") @GeneratedValue
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @param mixed $registrationDate
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     * @return mixed
     */
    public function getDateExpiresPremium()
    {
        return $this->dateExpiresPremium;
    }

    /**
     * @param mixed $dateExpiresPremium
     */
    public function setDateExpiresPremium($dateExpiresPremium)
    {
        $this->dateExpiresPremium = $dateExpiresPremium;
    }

    /**
     * @Column(type="string")
     */
    protected $password;

    /**
     * Электронная почта пользователя.
     * @Column(type="string", length=256)
     */
    protected $email;

    /**
     * Тип пользователя
     *
     * @Column (type="integer")
     */
    protected $type;

    /**
     * Дата создания регистрации.
     * @Column(type="string", nullable=true)
     */
    protected $registrationDate;

    /**
     * Дата Истечения премиум аккаунта.
     * @Column(type="string", nullable=true)
     */
    protected $dateExpiresPremium;

    /**
     * Список прикрепленных причин.
     *
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\UserCauseRelation", mappedBy="user")
     */
    protected $causes;

    /**
     * Список прикрепленных следствий.
     *
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\UserEffectRelation", mappedBy="effect")
     */
    protected $effects;

    /**
     * Возвращает
     * Массив полей Модели Player.
     *
     * @return array
     */
    final public function fields()
    {
        return array(
            "id"                 => 0,
            "email"              => '',
            "type"               => self::TYPE_DEFAULT,
            "registrationDate"   => "2015-01-02 10:01:00",
            "dateExpiresPremium" => "2015-01-02 10:01:00",
        );
    }

    /**
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getCauseRelations()
    {
        return $this->causes;
    }

    public function getCauses()
    {
        $result = array();
        $relations = $this->getCauseRelations()->getValues();

        /**
         * @var UserCauseRelation[] $relations
         */
        foreach ($relations as $relation) {

            $cause = $relation->getCause();
            $result []= $cause;
        }

        return $result;
    }

    /**
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getEffectRelations()
    {
        return $this->effects;
    }

    /**
     * Обновляет дату последнего изменения.
     * Обязателен при каждом изменении модели.
     */
    public function setDateTimeUpdate()
    {
        $date                 = new \DateTime('now');
        $this->lastDateUpdate = $date->format("Y-m-d H:i:s");
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function checkPassword($password)
    {
        return password_verify($password, $this->password);
    }
}

