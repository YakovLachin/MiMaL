<?php

namespace MiMaL\Core\User;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Cause\CausePresenter;
use MiMaL\Core\Relation\UserCauseRelation;
use MiMaL\Core\SecureLink\LinkSecuringInterface;
use MiMaL\Specification\SpecificationInterface;

/**
 * Отвечает за заполнение содержимым UserViewModel
 * и за то - какие данные можно отдавать клиенту.
 */
class UserPresenter
{
    /**
     * @var UserModel
     */
    protected $user;
    /**
     * @var bool
     */
    protected $withCauses;

    /**
     * @var bool
     */
    protected $withEffects;

    /**
     * @var CausePresenter
     */
    private $causePresenter;

    /**
     * @var SpecificationInterface
     */
    private $editSpecificator;
    /**
     * @var LinkSecuringInterface
     */
    private $linkSecuring;

    /**
     * UserPresenter constructor.
     *
     * @param CausePresenter         $causePresenter
     * @param SpecificationInterface $editSpecificator
     * @param LinkSecuringInterface  $linkSecuring
     */
    public function __construct(
        CausePresenter $causePresenter,
        SpecificationInterface $editSpecificator,
        LinkSecuringInterface $linkSecuring
     ) {
        $this->causePresenter   = $causePresenter;
        $this->editSpecificator = $editSpecificator;
        $this->linkSecuring     = $linkSecuring;
    }

    /**
     * @return UserViewModel
     */
    public function getView()
    {
        $user = $this->user;
        $viewModel = new UserViewModel();

        $viewModel->setId($user->getId());
        $viewModel->setEmail($user->getEmail());

        if ($this->withCauses) {
            $this->addCauses($viewModel, $user);
        }

        if ($this->withEffects)
        {}

        $this->addLinks($viewModel, $user);

        return $viewModel;
    }

    /**
     * @param UserModel $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param bool $withCauses
     * @return $this
     */
    public function setWithCauses($withCauses)
    {
        $this->withCauses = $withCauses;

        return $this;
    }

    /**
     * @param bool $withEffects
     * @return $this
     */
    public function setWithEffects($withEffects)
    {
        $this->withEffects = $withEffects;

        return $this;
    }

    /**
     * Добавляет в случае необходимости привязанные к пользователю причины.
     *
     * @param UserViewModel $viewModel
     * @param UserModel $user
     */
    private function addCauses(UserViewModel $viewModel, UserModel $user)
    {

        $causesModel = $user->getCauses();

        $causesView = array();
        foreach ($causesModel as $cause) {

            $cause = $this->causePresenter->setCause($cause)->getView();
            if ($cause) {
                $causesView[] = $cause;
            }
        }

        $viewModel->setCauses($causesView);
    }

    private function addLinks(UserViewModel $viewModel, UserModel $user)
    {
        $links = array(
            array(
                "rel"    => "read-with-causes",
                "method" => "",
                "link"   => "/api/user/" . $user->getId(). "/parent-causes",
            ),
        );

        if ($this->editSpecificator->isSatisfiedBy($user)) {
                $links [] = array(
                    "rel"    => "update",
                    "method" => "POST",
                    "link"   => $this->linkSecuring->generateSecureLink("/api/user" . $user->getId()),
                );
        }

        $viewModel->setLinks($links);
    }
}
