<?php

namespace MiMaL\Core\SecureLink;

use MiMaL\Encryption\CrypticInterface;
use MiMaL\Services\Site;
use Symfony\Component\HttpFoundation\Request;

class SecureLinkService implements LinkSecuringInterface
{
    /**
     * @var string
     */
    protected $secretKey;
    /**
     * @var CrypticInterface
     */
    private $crypter;
    /**
     * @var Site
     */
    private $siteService;

    /**
     * SecureLinkService constructor.
     * @param CrypticInterface $crypter
     * @param Site $siteService
     */
    public function __construct(CrypticInterface $crypter, Site $siteService)
    {
        $this->crypter     = $crypter;
        $this->siteService = $siteService;
    }

    public function requestBySecureLinkIsValid(Request $request)
    {
        $link         = new SecureLinkModel();
        $visitorToken = $this->generateVisitorToken();
        $link->setToken($request->get('token'));
        $link->setSalt($visitorToken);
        $this->crypter->decrypt($link);
        $path = $request->getPathInfo();

        $pathWithoutToken = strstr($path, "/" . $request->get('token'));
        if ($pathWithoutToken != $link->getUrl()) {
            return false;
        }

        return true;
    }

    public function generateSecureLink($path)
    {
        $link = new SecureLinkModel();
        $visitorToken = $this->generateVisitorToken();
        $link->setSalt($visitorToken);
        $link->setUrl($path);

        $this->crypter->encrypt($link);

        return $path . "/" . $link->getToken();
    }

    private function generateVisitorToken()
    {
        $visitor  = $this->siteService->getUser();

        $visitorToken = new VisitorTokenModel();
        $visitorToken->setVisitorId($visitor->getId());
        $visitorToken->setSalt($visitor->getEmail() . $visitor->getRegistrationDate() . $this->secretKey);
        $this->crypter->encrypt($visitorToken);

        return $visitorToken->getToken();
    }
}
