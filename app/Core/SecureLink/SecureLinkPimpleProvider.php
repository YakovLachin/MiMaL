<?php

namespace MiMaL\Core\SecureLink;

use MiMaL\Encryption\CrypticInterface;
use MiMaL\Kernel;
use MiMaL\Services\Site;
use Pimple\Container;

class SecureLinkPimpleProvider implements \Pimple\ServiceProviderInterface
{
    const SECURE_LINK           = 'secure.link';

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        /**
         * @use \Pimple\Container $pimple
         *
         * @return SecureLinkService
         */
        $pimple[self::SECURE_LINK] = function () use($pimple){
            /**
             * @var CrypticInterface $crypter
             */
            $crypter = $pimple->offsetGet(Kernel::CRYPTER);

            /**
             * @var Site $site
             */
            $site    = $pimple->offsetGet(Kernel::KERNEL_SITE);

            return new SecureLinkService($crypter, $site);
        };
    }
}
