<?php

namespace MiMaL\Core\SecureLink;

use Symfony\Component\HttpFoundation\Request;

interface LinkSecuringInterface
{
    /**
     * @param string $path
     *
     * @return string
     */
    public function generateSecureLink($path);

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function requestBySecureLinkIsValid(Request $request);
}
