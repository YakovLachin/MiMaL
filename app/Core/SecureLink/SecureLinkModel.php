<?php

namespace MiMaL\Core\SecureLink;

use MiMaL\Encryption\CryptedInterface;

class SecureLinkModel implements CryptedInterface
{
    protected $url;

    protected $salt;

    protected $token;

    public function getDecryptedData()
    {
        return  array (
            "url" => $this->url,
        );
    }

    public function setDecryptedData($data)
    {
        $this->url = $data["url"];
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}