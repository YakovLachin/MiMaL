<?php

namespace MiMaL\Core\SecureLink;

use MiMaL\Encryption\CryptedInterface;

class VisitorTokenModel implements CryptedInterface
{
    /**
     * @var int
     */
    protected $visitorId;


    protected $salt;

    protected $token;

    public function getDecryptedData()
    {
        $data = array(
            "visitorId"   => $this->visitorId,
        );

        return $data;
    }

    public function setDecryptedData($data)
    {
        $this->visitorId   = $data["visitorId"];
    }


    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getVisitorId()
    {
        return $this->visitorId;
    }

    /**
     * @param int $visitorId
     */
    public function setVisitorId($visitorId)
    {
        $this->visitorId = $visitorId;
    }
}