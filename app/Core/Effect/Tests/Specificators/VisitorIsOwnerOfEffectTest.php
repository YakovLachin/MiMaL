<?php

namespace MiMaL\Core\Effect\Tests\Specificators;

use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\Effect\Specificators\VisitorIsOwnerOfEffect;
use MiMaL\Core\User\UserModel;
use MiMaL\Services\Site;

class VisitorIsOwnerOfEffectTest extends \PHPUnit_Framework_TestCase
{
    public function test_isSatisfiedBy_UserIsOwner_ReturnTrue()
    {
        $visitor = new UserModel();
        $visitor->setId(1);
        $owner  = new UserModel();
        $owner->setId(1);
        $owners = array($owner);

        /**
         * @var Site|\PHPUnit_Framework_MockObject_MockObject $site
         */
        $site = $this->createMock(Site::class);
        $site->method($this->anything())->willReturn($visitor);
        $specificator = new VisitorIsOwnerOfEffect($site);
        $cause = $this->createMock(EffectModel::class);
        $cause->method('getUsers')->willReturn($owners);

        $this->assertTrue($specificator->isSatisfiedBy($cause));
    }

    public function test_isSatisfiedBy_UserIsNotOwner_ReturnFalse()
    {
        $visitor = new UserModel();
        $visitor->setId(1);
        $owner  = new UserModel();
        $owner->setId(2);
        $owners = array($owner);

        /**
         * @var Site|\PHPUnit_Framework_MockObject_MockObject $site
         */
        $site = $this->createMock(Site::class);
        $site->method($this->anything())->willReturn($visitor);
        $specificator = new VisitorIsOwnerOfEffect($site);
        $cause = $this->createMock(EffectModel::class);
        $cause->method('getUsers')->willReturn($owners);

        $this->assertFalse($specificator->isSatisfiedBy($cause));
    }
}
