<?php

namespace MiMaL\Core\Effect;

use MiMaL\Core\Effect\EffectModel;
use MiMaL\Services\DataBase;
use MiMaL\Models\Types;

/**
 * Менеджер для страниц
 * Class Entities
 *
 * @package MiMaL\managers
 */
class EffectService
{
    protected static $instance;

    /**
     * @var DataBase $dataBaseService
     */
    protected $dataBaseService;

    /**
     * @param DataBase $dataBaseService Сервис для работы с БД.
     */
    public function __construct(DataBase $dataBaseService)
    {
        $this->dataBaseService = $dataBaseService;
    }

    /**
     * @param DataBase $dataBaseService
     *
     * @return EffectService
     */
    public static function getInstance(DataBase $dataBaseService)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($dataBaseService);
        }

        return self::$instance;
    }

    /**
     * Возвращает модель сущности по идентификатору сущности.
     *
     * @param int $id Идентификатор сущности.
     *
     * @return EffectModel
     */
    public function readById($id)
    {
        /**
         * @var DataBase $service
         */
        $service    = $this->dataBaseService;
        $conditions = array(
            "id"         => $id,
        );
        $effect = $service->readEntity(Types::EFFECT, $conditions);

        return $effect;
    }

    /**
     * Удаляет модель сущности по идентификатору сущности.
     *
     * @param int $id Идентификатор сущности.
     *
     * @return mixed
     */
    public function deleteById($id)
    {
        /**
         * @var EffectModel $effect
         */
        $effect = $this->readById($id);
        try {
            $deletedEntity = $effect->rewriteModel(
                array(
                    "isDeleted" => true
                )
            );

            return $deletedEntity;
        } catch(\Exception $exception) {

            return $exception->getMessage();
        }
    }

    /**
     * Создает новую сущность исходя из переданных полей.
     *
     * @param $fields
     * @return mixed
     */
    public function create($fields)
    {
        $effect     = new EffectModel($fields);
        $effect->setDateTimeUpdate();
        $service = $this->dataBaseService;
        $result  = $service->writeEntity($effect);

        return $result;
    }

    /**
     * Обновляет сущность.
     *
     * @param int $id           Идентификатор сущности.
     * @param mixed[] $fields   Массив полей, которые будут перезаписаны.
     *
     * @return mixed
     */
    public function update($id, $fields)
    {
        /**
         * @var EffectModel $effect
         */
        $effect = $this->readById($id);
        $effect->rewriteModel($fields);
        $effect->setDateTimeUpdate();

        /**
         * @var DataBase $service
         */
        $service = $this->dataBaseService;
        /**
         * @var EffectModel $updatedEffect
         */
        $updatedEffect = $service->writeEntity($effect);

        return $updatedEffect;
    }

    public function readAncestorsById($id)
    {
        /**
         * @var EffectModel $effect
         */
        $effect  = $this->readById($id);

        /**
         * @var \Doctrine\ORM\PersistentCollection $relations
         */
        $collection = $effect->getAncestorRelationsCollection();
        $relations  = $collection->getValues();
        $ancestors  = array();
        /**
         * @var \MiMal\Core\Relation\EffectCauseRelation $relation
         */
        foreach ($relations as $relation) {
            $ancestors[]= $relation->getAncestor();
        }

        return $ancestors;
    }

    public function readDescendantsById($id)
    {
        /**
         * @var EffectModel $effect
         */
        $effect  = $this->readById($id);
        /**
         * @var \Doctrine\ORM\PersistentCollection $descendants
         */
        $collection  = $effect->getDescendantRelationsCollection();
        $relations   = $collection->getValues();
        $descendants = array();

        /**
         * @var \MiMal\Core\Relation\CauseEffectRelation $relation
         */
        foreach ($relations as $relation) {
            $descendants[]= $relation->getDescendant();
        }

        return $descendants;
    }
}
