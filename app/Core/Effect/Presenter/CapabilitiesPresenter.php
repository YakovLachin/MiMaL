<?php

namespace MiMaL\Core\Effect\Presenter;

use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\SecureLink\LinkSecuringInterface;
use MiMaL\Specification\SpecificationInterface;

class CapabilitiesPresenter
{
    /**
     * @var SpecificationInterface
     */
    private $ownerSpecificator;

    /**
     * @var EffectModel
     */
    protected $effect;
    /**
     * @var LinkSecuringInterface
     */
    private $linkSecuring;


    /**
     * CapabilitiesPresenter constructor.
     * @param SpecificationInterface $ownerSpecificator
     * @param LinkSecuringInterface $linkSecuring
     */
    public function __construct(
        SpecificationInterface $ownerSpecificator,
        LinkSecuringInterface $linkSecuring
    )
    {
        $this->ownerSpecificator = $ownerSpecificator;
        $this->linkSecuring      = $linkSecuring;
    }

    public function getView()
    {
        $capabilities = array();
        if ($this->ownerSpecificator->isSatisfiedBy($this->effect)) {
            $capabilities[] = array(
                "rel"    => "update",
                "method" => "POST",
                "link"   => $this->linkSecuring->generateSecureLink('/api/effect/' . $this->effect->getId())
            );
            $capabilities[] = array(
                "rel"    => "delete",
                "method" => "POST",
                "link"   => "",
            );
        }

        return $capabilities;
    }

    /**
     * @param EffectModel $effect
     * @return $this
     */
    public function setEffect($effect)
    {
        $this->effect = $effect;

        return $this;
    }
}