<?php

namespace MiMaL\Core\Effect;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Relation\UserEffectRelation;
use MiMaL\Core\User\UserModel;
use MiMaL\Models\DefaultModel;

/**
 * Модель страницы Статьи.
 *
 * @property EffectModel instance
 * @Entity @Table(name="effects")
 */
class EffectModel extends DefaultModel
{

    /**
     * int Идентификатор статьи.
     * @id @Column(type="integer", name="effect_id") @GeneratedValue
     */
    protected $id;


    /**
     * @var int Флаг - удалена ли сущность или нет.
     *
     * @Column (type="boolean", name="is_deleted")
     */
    protected $isDeleted;

    /**
     * string Заголовок статьи.
     * @Column(type="string")
     */
    protected $title;

    /**
     * string  Cодержимое сущности.
     * @Column(type="text")
     */
    protected $content;

    /**
     * Ссылка на сущность.
     * @Column(type="string", nullable=true)
     */
    protected $link;

    /**
     * Ссылка на превью сщуности.
     * @Column(type="string", name="preview_link", nullable=true)
     */
    protected $previewLink;

    /**
     * @var string Дата создания сушности.
     * @Column(type="integer", name="date_created", nullable=true)
     */
    protected $dateCreated;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\CauseEffectRelation", mappedBy="descendant")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $ancestors;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @OneToMany(targetEntity="\MiMaL\Core\Relation\EffectCauseRelation", mappedBy="ancestor")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $descendants;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     *
     * @var @OneToMany(targetEntity="\MiMaL\Core\Relation\UserEffectRelation", mappedBy="effect")
     * @JoinColumn(name="effect_id", referencedColumnName="effect_id")
     */
    protected $users;

    /**
     * Возвращает
     * Массив полей Модели Entity.
     *
     * @return array
     */
    final public function fields()
    {
        return array(
            "id"          => 0,
            "isDeleted"   => false,
            "title"       => '',
            "content"     => '',
            "link"        => '',
            "previewLink" => '',
            "dateCreated" => 0,
        );
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDateTimeUpdate()
    {
        $date = new \DateTime('now');
//        $this->date =$date->format("Y-m-d H:i:s");
//        $this->dateCreated = $date;
    }

    /**
     * Возвращает дату последних изменений.
     *
     * @return string
     */
    public function getDate()
    {
        return $this->dateCreated;
    }

    public function getAncestorRelationsCollection()
    {
        return $this->ancestors;
    }

    public function getDescendantRelationsCollection()
    {
        return $this->descendants;
    }

    /**
     * Возвращает массив потомков причин.
     *
     * @return CauseModel[]
     */
    public function getDescendants() {
        $relations = $this->descendants->getValues();

        $descendants = array();

        /**
         * @var \MiMaL\Core\Relation\EffectCauseRelation[] $relations
         */
        foreach ($relations as $relation) {
            $descendants[] = $relation->getDescendant();
        }

        return $descendants;
    }

    /**
     * Возвращает массив предков причин.
     *
     * @return CauseModel[]
     */
    public function getAncestors() {
        $relations = $this->ancestors->getValues();

        $ancestors = array();

        /**
         * @var \MiMaL\Core\Relation\CauseEffectRelation[] $relations
         */
        foreach ($relations as $relation) {
            $ancestors[] = $relation->getAncestor();
        }

        return $ancestors;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getPreviewLink()
    {
        return $this->previewLink;
    }

    /**
     * @param mixed $previewLink
     */
    public function setPreviewLink($previewLink)
    {
        $this->previewLink = $previewLink;
    }

    /**
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getUserRelationsCollection()
    {
        return $this->users;
    }

    /**
     * @return UserModel[]
     */
    public function getUsers() {
        $relations = $this->users->getValues();

        $users     = array();
        /**
         * @var UserEffectRelation[] $relations
         */
        foreach ($relations as $relation) {
            $users []= $relation->getUser();
        }

        return $users;
    }

}
