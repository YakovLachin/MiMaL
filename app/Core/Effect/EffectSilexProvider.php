<?php

namespace MiMaL\Core\Effect;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class EffectSilexProvider implements ControllerProviderInterface
{
    /**
     * Returns routes to connect to the given application.
     *
     * @param \Silex\Application $app An Application instance
     *
     * @return \Silex\ControllerCollection A ControllerCollection instance
     */
    public function connect(\Silex\Application $app)
    {
        /**
         * @var \Pimple\Container $container
         */
        $container = $app;

        $container->register(new EffectPimpleProvider());

        /**
         * @var \Silex\ControllerCollection $effects
         */
        $effects = $app['controllers_factory'];

        $effects->get('{id}', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var EffectService $service
             */
            $service = $container['effect.service'];
            $effect   = $service->readById($id);

            /**
             * @var \MiMaL\Core\Effect\EffectPresenter $presenter
             */
            $presenter = $container->offsetGet(EffectPimpleProvider::EFFECT_PRESENTER);
            $viewModel = $presenter->setEffect($effect)
                ->withAncestors(true)
                ->withDescendants(true)
                ->withCapabilities(true)
                ->getView();

            return $app->json(array("data" => $viewModel->getValues()));
        });

        $effects->get('{id}/descendants', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var EffectService $service
             */
            $service     = $container['effect.service'];
            $descendants = $service->readDescendantsById($id);
            $result      = array();

            foreach ($descendants as $descendant) {
                $result[] = $descendant->getValues();
            }

            return $app->json($result);
        });

        $effects->get('{id}/ancestors', function (Request $request) use ($container, $app) {
            $id  = $request->get('id');
            /**
             * @var EffectService $service
             */
            $service   = $container['effect.service'];
            $ancestors = $service->readAncestorsById($id);
            $result    = array();

            foreach ($ancestors as $ancestor) {
                $result[] = $ancestor->getValues();
            }

            return $app->json($result);
        });

        $effects->post('', function (Request $request) use ($container, $app) {
            $data  = $request->request->all();

            /**
             * @var EffectService $service
             */
            $service = $container['effect.service'];
            $effect   = $service->create($data);

            return $app->json($effect->getValues());
        });

        return $effects;
    }
}
