<?php

namespace MiMaL\Core\Effect;

use MiMaL\Core\Cause\CauseViewModel;
use MiMaL\Models\DefaultModel;

class EffectViewModel extends DefaultModel
{

    /**
     * @var int Идентификатор причины.
     */
    public $id;


    /**
     * @var bool Флаг - удалена ли сущность или нет.
     */
    protected $isDeleted;

    /**
     * @var string Заголовок причнины.
     */
    public $title;

    /**
     * @var string  Содержимое сущности.
     */
    public $content;

    /**
     * @var string Ссылка на сущность.
     */
    public $link;

    /**
     * @var string Ссылка на превью сущности.
     */
    public $previewLink;

    /**
     * @var string Дата создания сушности.
     */
    public $dateCreated;

    public $ancestors;
    public $descendants;
    public $capabilities;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getPreviewLink()
    {
        return $this->previewLink;
    }

    /**
     * @param mixed $previewLink
     */
    public function setPreviewLink($previewLink)
    {
        $this->previewLink = $previewLink;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed[]
     */
    public function fields()
    {
        return array(
            "id"          => 0,
            "isDeleted"   => false,
            "title"       => '',
            "content"     => '',
            "link"        => '',
            "previewLink" => '',
            "ancestors"    => array(),
            "descendants"  => array(),
            "dateCreated" => 0,
            "capabilities"       => array(),
        );
    }

    /**
     * @param  string $time
     * @return string
     */
    public function setDateTimeUpdate()
    {
        // TODO: Implement setDateTimeUpdate() method.
    }

    /**
     * @param CauseViewModel[] $ancestors
     */
    public function setAncestors($ancestors)
    {
        $this->ancestors = $ancestors;
    }

    /**
     * @param CauseViewModel[] $descendants
     */
    public function setDescendants($descendants)
    {
        $this->descendants = $descendants;
    }

    /**
     * @param array $capabilities
     */
    public function setCapabilities($capabilities)
    {
        $this->capabilities = $capabilities;
    }
}