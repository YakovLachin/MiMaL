<?php

namespace MiMaL\Core\Effect;

use MiMaL\Core\Cause\CausePimpleProvider;
use MiMaL\Core\Effect\Specificators\VisitorIsOwnerOfEffect;
use MiMaL\Core\SecureLink\SecureLinkPimpleProvider;
use MiMaL\Kernel;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class EffectPimpleProvider implements ServiceProviderInterface
{
    const EFFECT_PRESENTER = 'effect.presenter';
    const EFFECT_SPECIFICATOR_VISITOR_IS_OWNER = 'efffect.specificator.visitor_is_owner_of_effect';
    const EFFECT_CAPABILITIES_PRESENTER = 'effect.capabilities.presenter';

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param \Pimple\Container $pimple A container instance
     */
    public function register(\Pimple\Container $pimple)
    {
        $pimple['effect.service'] = function($pimple) {
            $dataBaseService = $pimple['dataService'];

            return new EffectService($dataBaseService);
        };

        $pimple[self::EFFECT_PRESENTER ] = $pimple->factory(function ($pimple) {
            return new EffectPresenter($pimple);
        });

        $pimple[self::EFFECT_CAPABILITIES_PRESENTER] = $pimple->factory(function ($pimple) {

            /**
             * @var Container $pimple
             */
            $ownerSpecificator = $pimple->offsetGet(self::EFFECT_SPECIFICATOR_VISITOR_IS_OWNER);
            $secureLinkService = $pimple->offsetGet(SecureLinkPimpleProvider::SECURE_LINK);

            return new \MiMaL\Core\Effect\Presenter\CapabilitiesPresenter($ownerSpecificator, $secureLinkService);
        });

        /**
         * @param Container $pimple
         * @return VisitorIsOwnerOfEffect
         */
        $pimple[self::EFFECT_SPECIFICATOR_VISITOR_IS_OWNER] = function ($pimple) {
            /**
             * @var \MiMaL\Services\Site $site
             */
            $site = $pimple->offsetGet(Kernel::KERNEL_SITE);

            return new VisitorIsOwnerOfEffect($site);
        };


    }
}
