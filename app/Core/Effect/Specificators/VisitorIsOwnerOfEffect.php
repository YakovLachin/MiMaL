<?php

namespace MiMaL\Core\Effect\Specificators;

use MiMaL\Core\Effect\EffectModel;
use MiMaL\Core\Relation\UserEffectRelation;
use MiMaL\Core\User\UserModel;
use MiMaL\Services\Site;
use MiMaL\Specification\CompositeSpecification;

class VisitorIsOwnerOfEffect extends CompositeSpecification
{
    /**
     * @var Site
     */
    private $site;

    /**
     * VisitorIsOwnerOfCause constructor.
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    public function isSatisfiedBy($effect)
    {
        /**
         * @var EffectModel $effect
         * @var UserModel[] $users
         */
        $users = $effect->getUsers();
        $visitor = $this->site->getUser();
        if (null == $visitor) {
            return false;
        }

        foreach ($users as $user) {
            if ($user->getId() == $visitor->getId()) {
                return true;
            }
        }

        return false;
    }
}
