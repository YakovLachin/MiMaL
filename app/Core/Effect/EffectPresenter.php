<?php

namespace MiMaL\Core\Effect;

use MiMaL\Core\Cause\CauseModel;
use MiMaL\Core\Cause\CausePimpleProvider;
use MiMaL\Core\Cause\CausePresenter;
use MiMaL\Core\Effect\Presenter\CapabilitiesPresenter;

class EffectPresenter
{
    /**
     * @var EffectModel
     */
    protected $effect;

    /**
     * @var bool
     */
    protected $withAncestors;

    /**
     * @var bool
     */
    protected $withDescendants;

    /**
     * @var bool
     */
    protected $withCapabilities;
    /**
     * @var \Pimple\Container $container
     */
    protected $container;

    /**
     * EffectPresenter constructor.
     * @param $container
     */
    public function __construct(\Pimple\Container $container)
    {
        $this->container = $container;
    }

    public function getView()
    {
        $result = new EffectViewModel();
        $result->setId($this->effect->getId());
        $result->setTitle($this->effect->getTitle());
        $result->setContent($this->effect->getContent());
        $result->setLink($this->effect->getLink());
        $result->setPreviewLink($this->effect->getPreviewLink());

        if ($this->withCapabilities) {
            /**
             * @var CapabilitiesPresenter $capabilitiesPresenter
             */
            $capabilitiesPresenter = $this->container->offsetGet(EffectPimpleProvider::EFFECT_CAPABILITIES_PRESENTER);
            $capabilities = $capabilitiesPresenter->setEffect($this->effect)->getView();
            $result->setCapabilities($capabilities);

        }

        if ($this->withAncestors) {
            $this->addAncestors($result);
        }

        if ($this->withDescendants) {
            $this->addDescendants($result);
        }

        return $result;
    }

    /**
     * @param EffectModel $effect
     * @return $this
     */
    public function setEffect(EffectModel $effect)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * @param bool $withAncestors
     *
     * @return $this
     */
    public function withAncestors($withAncestors)
    {
        $this->withAncestors = $withAncestors;

        return $this;
    }

    /**
     * @param bool $withDescendants
     *
     * @return $this
     */
    public function withDescendants($withDescendants)
    {
        $this->withDescendants= $withDescendants;

        return $this;
    }

    private function addAncestors(EffectViewModel $viewModel)
    {
        /**
         * @var CausePresenter $causePresenter
         */
        $causePresenter = $this->container->offsetGet(CausePimpleProvider::CAUSE_PRESENTER);
        $ancestors = $this->effect->getAncestors();

        $views = array();

        /**
         * @var CauseModel[] $ancestors
         */
        foreach ($ancestors as $ancestor) {
            $ancestorView = $causePresenter->setCause($ancestor)->getView();
            $views []= $ancestorView;
        }

        $viewModel->setAncestors($views);
    }

    private function addDescendants(EffectViewModel  $viewModel)
    {
        /**
         * @var CausePresenter $causePresenter
         */
        $causePresenter = $this->container->offsetGet(CausePimpleProvider::CAUSE_PRESENTER);
        $descendants    = $this->effect->getDescendants();

        $views = array();

        /**
         * @var CauseModel[] $descendants
         */
        foreach ($descendants as $descendant) {
            $ancestorView = $causePresenter->setCause($descendant)->getView();
            $views []= $ancestorView;
        }

        $viewModel->setDescendants($views);
    }

    /**
     * @param bool $withCapabilities
     *
     * @return $this
     */
    public function withCapabilities($withCapabilities)
    {
        $this->withCapabilities = $withCapabilities;

        return $this;
    }

}