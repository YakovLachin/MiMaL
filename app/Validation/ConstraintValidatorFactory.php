<?php

namespace MiMaL\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorFactory as BaseConstraintValidatorFactory;

class ConstraintValidatorFactory extends BaseConstraintValidatorFactory
{

    /**
     * ConstraintValidatorFactory constructor.
     * @param $validators
     */
    public function __construct($validators)
    {

        $this->validators = $validators;
    }

    public function getInstance(Constraint $constraint)
    {
        return parent::getInstance($constraint);
    }
}
