<?php
namespace MiMaL\Validation\Validators\Constraints;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class PageConstraint implements ConstraintInterface
{
    private $id;


    /**
     * Записывает Текущее значение для валидации.
     *
     * @param $value
     */
    public function setValue($value)
    {
        $this->id = $value;
    }

    /**
     * Метод где, определяются правила.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('id', new Assert\Range(
            array('min' => 1, 'max' => 50)
        ));
    }
}
