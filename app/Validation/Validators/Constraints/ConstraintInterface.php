<?php

namespace MiMaL\Validation\Validators\Constraints;

use Symfony\Component\Validator\Mapping\ClassMetadata;

interface ConstraintInterface
{
    /**
     * Записывает данные для валидации.
     *
     * @param $value
     */
    public function setValue($value);

    /**
     * Метод где, определяются правила.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata);
}
