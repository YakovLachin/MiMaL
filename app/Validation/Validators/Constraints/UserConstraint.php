<?php

namespace MiMaL\Validation\Validators\Constraints;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class UserConstraint implements ConstraintInterface
{
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    protected $password;
    protected $value;

    /**
     * Записывает данные для валидации.
     *
     * @param $value
     */
    public function setValue($value)
    {
        $this->email = $value['email'];
        $this->password = $value['password'];
    }

    /**
     * Метод где, определяются правила.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('email', array(
            new Assert\NotNull(),
            new Assert\NotBlank(),
            new Assert\Email(),
            new Assert\Length(array(
                "max" => 250,
                "min" => 7
            ))
        ));
        $metadata->addPropertyConstraints('password', array(
            new Assert\NotNull(),
            new Assert\NotBlank(),
            new Assert\Regex(array(
                "pattern" => '/^[\w\d]+$/'
            )),
            new Assert\Length(array(
                "max" => 250,
                "min" => 6
            ))
        ));
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
