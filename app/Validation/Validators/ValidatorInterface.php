<?php

namespace MiMaL\Validation\Validators;

interface ValidatorInterface
{
    /**
     * Вилидирует значение и возвращает результат.
     *
     * @param  mixed $value Значение требуемое для валидации.
     * @return bool
     */
    public function isValid($value);
}
