<?php

namespace MiMaL\Validation\Validators;

use MiMaL\Validation\Validators\Constraints\ConstraintInterface;
use MiMaL\Validation\Validators\Constraints\Page;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class BaseSymfonyValidator
{
    /**
     * @var ConstraintInterface
     */
    protected $constraint;
    protected $error;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Entity constructor.
     * @param $validator
     */
    public function __construct($validator)
    {
        $this->validator = $validator;
    }

    /**
     * Вилидирует значение и возвращает результат.
     *
     * @param  mixed $value Значение требуемое для валидации.
     * @return bool
     */
    public function isValid($value)
    {
        $this->constraint->setValue($value);
        $error = $this->validator->validate($this->constraint);
        /**
         * @var ConstraintViolationList $error
         */
        if ($error->count()) {
            $this->error = $error;

            return false;
        }

        return true;
    }

    /**
     * @param ConstraintInterface $constraint
     */
    public function setConstraint($constraint)
    {
        $this->constraint = $constraint;
    }

    /**
     * Возвращает сообщение о не валидных данных.
     * @var ConstraintViolationList $error
     * @return string
     */
    public function getMessage()
    {
        /**
         * @var ConstraintViolation $violation
         */
        $violation = $this->error->get(0);
        return $violation->getMessage();
    }
}
