<?php

namespace MiMaL\Validation;

use MiMaL\Validation\Validators\BaseSymfonyValidator;
use MiMaL\Validation\Validators\Constraints\PageConstraint;
use MiMaL\Validation\Validators\Constraints\UserConstraint;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Provider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $container                                = new Container();
        $container['validator.validator_factory'] = function ($container) {
            $validators = $container['validator.validator_service_ids'];

            return new ConstraintValidatorFactory($validators);
        };

        $container['symfony.validator'] = function ($container) {
            /**
             * @var ValidatorInterface The new validator
             */
            $builder           = \Symfony\Component\Validator\Validation::createValidatorBuilder(
            );
            $constraintFactory = $container["validator.validator_factory"];
            $builder->setConstraintValidatorFactory($constraintFactory);
            $validator = $builder->addMethodMapping(
                'loadValidatorMetadata'
            )->getValidator();

            return $validator;
        };

        $container['validator.validator_service_ids'] = function () {
            return array(
                'Symfony\Component\Validator\Constraints\RangeValidation'      => new \Symfony\Component\Validator\Constraints\RangeValidator,
                'Symfony\Component\Validator\Constraints\RegexValidation'      => new \Symfony\Component\Validator\Constraints\RegexValidator,
                'Symfony\Component\Validator\Constraints\TypeValidation'       => new \Symfony\Component\Validator\Constraints\TypeValidator,
                'Symfony\Component\Validator\Constraints\NotNullValidation'    => new \Symfony\Component\Validator\Constraints\NotNullValidator,
                'Symfony\Component\Validator\Constraints\EmailValidation'      => new \Symfony\Component\Validator\Constraints\EmailValidator,
                'Symfony\Component\Validator\Constraints\CollectionValidation' => new \Symfony\Component\Validator\Constraints\CollectionValidator,
                'Symfony\Component\Validator\Constraints\LengthValidation'     => new \Symfony\Component\Validator\Constraints\LengthValidator,
                'Symfony\Component\Validator\Constraints\NotBlankValidation'   => new \Symfony\Component\Validator\Constraints\NotBlankValidator,
            );
        };

        $pimple['validator.page'] = function ($pimple) use ($container) {
            /**
             * @var ValidatorInterface $symfonyValidator
             */
            $symfonyValidator = $container['symfony.validator'];

            $baseValidator = new BaseSymfonyValidator($symfonyValidator);
            $constraint    = new PageConstraint();
            $baseValidator->setConstraint($constraint);

            return $baseValidator;
        };

        $pimple['validator.user'] = function ($pimple) use ($container) {
            /**
             * @var ValidatorInterface $symfonyValidator
             */
            $symfonyValidator = $container['symfony.validator'];

            $baseValidator = new BaseSymfonyValidator($symfonyValidator);
            $constraint    = new UserConstraint();
            $baseValidator->setConstraint($constraint);

            return $baseValidator;
        };
    }
}
