<?php

namespace MiMaL\Encryption;

interface HasDecryptedDataInterface
{
    public function getDecryptedData();
    public function setDecryptedData($data);
}
