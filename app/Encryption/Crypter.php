<?php

namespace MiMaL\Encryption;

use Firebase\JWT\JWT;

class Crypter implements CrypticInterface
{
    /**
     * @var JWT
     */
    private $jwt;

    /**
     * @var string
     */
    private $secretKey;

    /**
     * Encrypter constructor.
     *
     * @param JWT $jwt
     * @param     $secretKey
     */
    public function __construct(JWT $jwt, $secretKey)
    {
        $this->jwt       = $jwt;
        $this->secretKey = $secretKey;
    }

    /**
     * @param CryptedInterface $encrypted
     *
     * @return mixed
     */
    public function encrypt(CryptedInterface $encrypted)
    {
        $salt = $encrypted->getSalt();
        $data = $encrypted->getDecryptedData();

        return $encrypted->setToken(JWT::encode($data, $salt . $this->secretKey, 'HS256'));
    }

    /**
     * @param CryptedInterface $decrypted
     *
     * @return mixed
     */
    public function decrypt(CryptedInterface $decrypted)
    {
        $salt = $decrypted->getSalt();
        $token = $decrypted->getToken();

        return $decrypted->setDecryptedData(JWT::decode($token, $salt . $this->secretKey, array('HS256')));
    }
}
