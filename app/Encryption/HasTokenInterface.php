<?php

namespace MiMaL\Encryption;

interface HasTokenInterface
{
    /**
     * @return string
     */
    public function getToken();

    /**
     * @param string
     */
    public function setToken($string);
}
