<?php

namespace MiMaL\Encryption;

interface HasSaltInterface
{
    /**
     * @return string
     */
    public function getSalt();

    /**
     * @param string $salt
     */
    public function setSalt($salt);
}
