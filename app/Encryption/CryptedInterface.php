<?php

namespace MiMaL\Encryption;

interface CryptedInterface extends HasTokenInterface, HasSaltInterface, HasDecryptedDataInterface
{}
