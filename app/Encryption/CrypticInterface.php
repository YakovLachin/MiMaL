<?php

namespace MiMaL\Encryption;

interface CrypticInterface
{
    public function encrypt(CryptedInterface $encrypted);
    public function decrypt(CryptedInterface $encrypted);
}
