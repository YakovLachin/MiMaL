<?php

require_once __DIR__ . '/../vendor/autoload.php';

use MiMaL\Core\Cause\CauseSilexProvider;
use MiMaL\Core\Effect\EffectSilexProvider;
use MiMaL\Core\User\UserSilexProvider;
use MiMaL\Services\Security;
use MiMaL\Services\Site;
use MiMaL\Validation\Validators\BaseSymfonyValidator;
use Symfony\Component\HttpFoundation\Request;
use MiMaL\Kernel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

$app               = new Silex\Application();
$app ['debug']     = true;
$app->register(new Kernel());
$app->before(function(Request $request) use ($app) {
    $uri        = $request->getUri();
    $host       = $request->getHost();
    $allowedUri = array (
        "signup" => "http://" . $host . "/api/signup",
        "signin" => "http://" . $host . "/api/signin"
    );
    if (in_array($uri, $allowedUri)) {
        return;
    }


        if ($request->cookies->get('Mimal-Access-Token')) {
        /**
         * @var \MiMaL\Services\Security $serviceSecurity
         */
        $serviceSecurity = $app->offsetGet("serviceSecurity");
        $accessToken     = $request->cookies->get('Mimal-Access-Token');
        if (!$serviceSecurity->auntificateByToken($accessToken)) {
            throw new \MiMaL\Errors\AccessDenied;
        }

        return;
    }

    throw new \MiMaL\Errors\AccessDenied;
});

$app->after(function (Request $request, Response $response) use ($app) {

    /**
     * @var Site $siteService
     */
    $siteService = $app->offsetGet(Kernel::KERNEL_SITE);

    $expire = new \DateTime();
    $expire->modify("+1 week");

    $token = $siteService->getUserToken();

    if (!empty($token)) {
        $cookie = new Cookie("Mimal-Access-Token", $token , $expire, '');
        $response->headers->setCookie($cookie);
    }
});

//$app->error(function (\Exception $e, Request $request, $code) {
//    switch ($code) {
//        case 403:
//            $message = 'Acess Denied.';
//            break;
//        case 404:
//            $message = 'The requested page could not be found.';
//            break;
//        case 500:
//            $message = 'The Internal Error';
//            break;
//        default:
//            $message = 'We are sorry, but something went terribly wrong.';
//    }
//
//    return new Response($message);
//});
/**
 * Авторизация пользователя.
 * url: "/signin"
 */
$app->post('/api/signin', function (Request $request) use ($app) {
    $data  = json_decode($request->getContent(), true);

    /**
     * @var MiMaL\Validation\Validators\BaseSymfonyValidator $validator
     */
    $validator = $app->offsetGet('validator.user');

    if (!$validator->isValid($data)) {
        throw new \MiMaL\Errors\Users\AuthorizeError($validator->getMessage());
    }

    /**
     * @var BaseSymfonyValidator $validator
     */
    $validator = $app->offsetGet('validator.user');
    if (!$validator->isValid($data)) {
        throw  new Exception("invalid params");
    }

    /**
     * @var Security $serviceSecurity
     */
    $serviceSecurity = $app->offsetGet("serviceSecurity");
    try {
        $serviceSecurity->authorize($data["email"], $data["password"]);
        /**
         * @var Site $serviceSite
         */
        $serviceSite = $app->offsetGet("serviceSite");
        $user        = $serviceSite->getUser();
        $token       = $serviceSite->getUserToken();
        $response    = array(
            "data" => $user->toArray(),

            "meta" => array(
                "token" => $token,
            ),
        );

        return json_encode($response);


    } catch (Exception $e) {
        return $e->getMessage();
    }
}
);

/**
 * Регистрация
 * url: "/signup"
 */
$app->post(
    '/api/signup', function (Request $request) use ($app) {
    $data  = json_decode($request->getContent(), true);

    /**
     * @var MiMaL\Validation\Validators\BaseSymfonyValidator $validator
     */
    $validator = $app->offsetGet('validator.user');

    if (!$validator->isValid($data)) {
        throw new \MiMaL\Errors\Users\AuthorizeError($validator->getMessage());
    }

    /**
     * @var Security $security
     */
    $security = $app->offsetGet("serviceSecurity");
    try {
        $security->registrate($data["email"], $data['password']);
        /**
         * @var Site $serviceSite
         */
        $serviceSite = $app->offsetGet("serviceSite");
        $user        = $serviceSite->getUser();
        $token       = $serviceSite->getUserToken();
        $response    = array(
            "data" => $user->toArray(),
            "meta" => array(
                "token" => $token,
            ),
        );

        return json_encode($response);
    } catch (Exception $e) {
        return $e->getMessage();
    }
}
);

$app->mount("/api/cause", new CauseSilexProvider());
$app->mount("/api/effect", new EffectSilexProvider());
$app->mount("/api/user", new UserSilexProvider());

return $app;
