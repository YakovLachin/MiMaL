<?php

namespace MiMaL\Models;

/**
 * Абстрактный класc модели. Все Модели наследуются от этого класса и
 * переопределяют методы отвечающие за установку типа сущности модели.
 *
 * Class DefaultModel
 */
abstract class DefaultModel
{
    /**
     * Конструктор класса.
     * @param mixed[] $fields Массив передаваемых полей при создании модели.
     */
    public function __construct($fields = array())
    {
        $standartFields = $this->fields();
        foreach ($standartFields as $key => $value) {
            $this->$key = $value;
        }

        if (!empty($fields)) {
            return $this->rewriteModel($fields);
        }
    }

    /**
     * Перезаписывает Модель.
     *
     * @param  mixed[] $newFields Массив значений.
     * @return $this
     */
    public function rewriteModel($newFields)
    {
        $fields  = $this->fields();
        foreach ($newFields as $key => $value) {
            if (array_key_exists($key, $fields)) {
                if (is_array($value)) {
                    $this->$key = json_encode($value);
                } else {
                    $this->$key = $value;
                }
            }
        }

        return $this;
    }

    /**
     * Возвращает массив полей модели.
     *
     * @return array
     */
    public function toArray()
    {
        $arrayFields = array();
        foreach ($this as $key => $value) {
            $arrayFields[(string)$key]  = $value;
        }
        return $arrayFields;
    }

    /**
     * Сохраняет модель в Б.Д.
     */
    public function save()
    {
        $fields  = $this->toArray();
        $manager = $this->manager();
        $result  = false;
        if (0 === (int)$fields["id"]) {
            $result = $manager->create($this);
        }

        if (0 !== (int)$fields["id"]) {
            $result = $manager->update($this);
        }

        return $result;
    }

    public function getValues()
    {
        $result = array();

        foreach ($this->fields() as $key => $value) {
            if (!isset($this->$key) || empty($this->$key)) {
                $result[$key] = $value;
            } elseif (is_bool($key)) {
                $result[$key] = (bool)$this->$key;
            } elseif (is_array($key)) {
                $result[$key] = json_decode($this->$key);
            } else {
                $result[$key] = $this->$key;
            }
        }

        return $result;
    }

    /**
     * @return mixed[]
     */
    abstract public function fields();
    /**
     * @param  string $time
     * @return string
     */
    abstract public function setDateTimeUpdate();
}
