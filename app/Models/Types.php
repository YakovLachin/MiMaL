<?php

namespace MiMaL\Models;

class Types
{
    /**
     * Пользователь
     */
    const USER   = 2;

    /**
     * Причина
     */
    const CAUSE   = 3;

    /**
     * Следствие
     */
    const EFFECT  = 4;

    /**
     * Связь Причины(Как родителя) и Следствия(Как потомка)
     */
    const CAUSE_TO_EFFECT_RELATION = 5;

    /**
     * Связь Следствия(Как родителя) и Причины(Как потомка)
     */
    const EFFECT_TO_CAUSE_RELATION = 6;

    /**
     * Связь Пользователя и причины.
     */
    const USER_TO_CAUSE_RELATION = 7;

    /**
     * Связь Пользователя и Следствия
     */
    const USER_TO_EFFECT_RELATION = 8;
}
